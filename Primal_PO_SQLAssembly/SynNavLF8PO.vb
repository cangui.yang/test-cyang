﻿Imports System.Data.SqlClient
Imports System.Net

Public Class SynNavLF8PO
    <Microsoft.SqlServer.Server.SqlProcedure>
    Public Shared Sub UpdatePurchaseOrderLine(ByVal domain As String, ByVal username As String, ByVal password As String,
                                          ByVal PurchaseOrderNo As String, ByVal ProductCode As String, ByVal UOMC As String, ByVal Quantity As Decimal,
                                          ByVal ConsignedQuantity As Decimal, ByVal QtytoReceived As Decimal, ByVal QtytoInvoice As Decimal)
        Dim Credentials As NetworkCredential = New NetworkCredential(username, password, domain)
        Dim POLineUpdate As lf8nav.PurchaseOrderWS = New lf8nav.PurchaseOrderWS
        POLineUpdate.Credentials = Credentials
        POLineUpdate.UpdatePurchaseLine(PurchaseOrderNo, ProductCode, UOMC, Quantity, ConsignedQuantity, QtytoReceived, QtytoInvoice)
        POLineUpdate.Dispose()
    End Sub

End Class
