﻿<%@ Page Title="" Language="VB" MasterPageFile="~/LambInterface.master" AutoEventWireup="false" CodeFile="lambInterface_bookpermits.aspx.vb" Inherits="lambInterface_bookpermits" EnableEventValidation="false" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />--%>
    <script>
        var scrolled = 0;
        var modal_SupplierScolled = 0;
        var controlIDAcceptValue;
        function PickSpecieValidation() {
            var tf = true ;
            if ($('#<%=DropDownList_Model_PickSpecie_Destination.ClientID %> option:selected').text() == "Blank") {
                document.getElementById("PickSpecie_modal_Destination_alert").style.display = "Block";
                tf= false;
            }
            else {
                document.getElementById("PickSpecie_modal_Destination_alert").style.display = "None";

            }
          
            if (document.getElementById('<%=CheckBox_12months.ClientID %>').checked) {
                document.getElementById("PickSpecie_modal_CheckBox12months_alert").style.display = "none";

            }
            else {
                document.getElementById("PickSpecie_modal_CheckBox12months_alert").style.display = "Block";
                tf= false;
            }

            if (document.getElementById('<%=CheckBox_30days.ClientID %>').checked) {
                document.getElementById("PickSpecie_modal_CheckBox30days_alert").style.display = "none";
            }
            else {
                document.getElementById("PickSpecie_modal_CheckBox30days_alert").style.display = "Block";
                tf=false;
            }

            if (document.getElementById('<%=CheckBox_3farm.ClientID %>').checked) {
                document.getElementById("PickSpecie_modal_CheckBox3farm_alert").style.display = "none";
            }
            else {
                document.getElementById("PickSpecie_modal_CheckBox3farm_alert").style.display = "Block";
                tf= false;
            }

            return tf;

        }
 
        function KeyPaysetValue(val) {
           
            try {

                document.getElementById(controlIDAcceptValue).value = document.getElementById(controlIDAcceptValue).value + val;
                document.getElementById(controlIDAcceptValue).focus();
            }
            catch (err) {
                $('#modal_Error_alert').show();
             //   alert("Please set focus on text box to accept key pad value.");
                document.getElementById('Label_Modal_AlertMessage').innerText = " Please set focus on text box to accept key pad value."               
            }
           
        }
        function Modal_AlertMessageSuccess(id, t) {
            document.getElementById(id).innerText = t;
            $('#modal_Confirm_alert').show();
        }

        function Modal_AlertMessage(id,t) {
            document.getElementById(id).innerText = t;
            $('#modal_Error_alert').show();
        }
        function Modal_AlertHide(id) {
            document.getElementById(id).innerText ='';
            $('#modal_Error_alert').hide();
        }

        function setFocusID(id) {
            controlIDAcceptValue = id;
        }
        function ketPayDelete() {
            var currentValue = document.getElementById(controlIDAcceptValue).value;
            currentValue = currentValue.substring(0, currentValue.length - 1);
            document.getElementById(controlIDAcceptValue).value = currentValue;
        }

        function ScollUp(control,value) {
            if (scrolled > 0) {
                scrolled = scrolled - value;
                $(control).animate({
                    scrollTop: scrolled
                });
            }
        }
        function ScollDown(control, value) {
            scrolled = scrolled + value;
            $(control).animate({
                scrollTop: scrolled
            });
        }
        maintainScrollPosition("#GridviewDiv", '#<%=dv_overView_scoll.ClientID %>');
        maintainScrollPosition("#Gridview_SupplierDiv", '#<%=HiddenField_Modal_Supplier.ClientID %>');

        $(document).ready(function () {
           
            //$('ul li a').click(function () {
            //    $('ul li.active').removeClass('active');
            //    $(this).closest('li').addClass('active');
            //});
            //$('#Button_Enter').click(function () {
            //    var e = $.Event("keypress", { which: 13 });
            //   this.trigger(e);
            //});

            //document.getElementById("PickSpecie_modal_Destination_alert").style.display = "none";
            //document.getElementById("PickSpecie_modal_CheckBox12months_alert").style.display = "none";
            //document.getElementById("PickSpecie_modal_CheckBox30days_alert").style.display = "none";
            //document.getElementById("PickSpecie_modal_CheckBox3farm_alert").style.display = "none";

            $(".KeyPadinputSting").click(function () {
                controlIDAcceptValue = $(this).attr("id");
               // alert(controlIDAcceptValue);
            });

            $('#modal_Error_alert').hide();
            $('.alert .close').on('click', function (e) {
               $(this).parent().hide();
            });

           


            
            $("#Button_Up").on("click", function () {
                if (scrolled > 0) {
                    scrolled = scrolled - 30;
                    $("#GridviewDiv").animate({
                        scrollTop: scrolled
                    });
                }
                
            });
          
            $("#Button_Down").on("click", function () {
              
                    scrolled = scrolled +30;
                    $("#GridviewDiv").animate({
                        scrollTop: scrolled
                    });
                 
            });


     

        });


         function pageLoad() {
             maintainScrollPosition("#GridviewDiv", '#<%=dv_overView_scoll.ClientID %>');
             maintainScrollPosition("#Gridview_SupplierDiv", '#<%=HiddenField_Modal_Supplier.ClientID %>');
             
             }
        function maintainScrollPosition(divcontrol, PositionControl) {
            $(divcontrol).scrollTop($(PositionControl).val());
        }

        function setScrollPosition(scrollValue, control) {
            $(control).val(scrollValue);
            scrolled = scrollValue;
        }
        function setSupplierScrollPosition(scrollValue, control) {
            $(control).val(scrollValue);
            modal_SupplierScolled = scrollValue;
        
        }

</script>
    <style>
    .KeyBoard {display:none;}
        .KeyBoardStyle {
              width:65px;
              height:52px;
              margin-bottom:5px;
              font-size:large;
        }
        .KeyBoardStyle_Alphabet {
              width:50px;
              height:45px;
              font-size:large;
        }
        .auto-style1 {
            position: relative;
            min-height: 1px;
            float: left;
            width: 33.33333333%;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
                <div class="container">
                    <div class="form-inline">
                            <div class="form-group" style="margin-bottom:5px">
         <label>Lairage Date:</label> &nbsp;<asp:Button ID="Button_pre" runat="server" Text="<<" class="btn btn-default" />
            <asp:TextBox ID="TextBox_Date" runat="server" class="form-control" AutoPostBack="True" Width="150px"> 
            </asp:TextBox>&nbsp;<asp:Button ID="Button_next" runat="server" Text=">>" class="btn btn-default" />
            <input id="Button_Up" type="button" value="UP" class="btn btn-info" style="width:70px" onclick="ScollUp('GridviewDiv',45)"  />
           &nbsp;<input id="Button_Down" type="button" value="Down" class="btn btn-info"  style="width:70px" onclick="ScollDown('GridviewDiv', 45)"   />
            <ajaxToolkit:CalendarExtender ID="TextBox_Date_CalendarExtender" runat="server" BehaviorID="TextBox_Date_CalendarExtender" Format="dd/MM/yyyy" TargetControlID="TextBox_Date" TodaysDateFormat="d MMMM, yyyy">
            </ajaxToolkit:CalendarExtender>&nbsp;
 <asp:Button ID="Button_NewLot" runat="server" Text="New Lot" Width="96px" class="btn btn-info btn-m" data-toggle="modal" data-target="#myModal" />&nbsp;
<%--  <asp:Button ID="Button_Back" runat="server" Text="Back" Width="96px" class="btn btn-info btn-m"  />&nbsp;--%>
            <asp:Label ID="Label1" runat="server" Text="Total:"></asp:Label><asp:Label ID="Label_TotalLambs" runat="server" Text=""></asp:Label>
       </div>
                    </div> 
                    <asp:HiddenField ID="dv_overView_scoll" Value="0" runat="server" />
                    <table  style="width:1010px; text-align:left; font-size:15px; font-weight:600">
            <tr class="w3-table w3-striped w3-bordered w3-card-4 w3-blue" >
                <td style="width:60px;" ></td>
                <td style="width:140px;">PermitNo</td>
                <td style="width:110px;">FlockNo</td>
                <td style="width:280px;">Supp Name</td>
                <td style="width:50px;">QA</td>
<%--                <td style="width:100px;">Cert No</td>--%>
                <td style="width:80px;">Desc</td>
                <td style="width:50px;">Reg</td>
                <td style="width:50px;">VO</td>
                <td style="width:50px;">Qty</td>
                <td style="width:50px;">SFS</td>
            </tr>

        </table>
                    <div id="GridviewDiv"  style="overflow-y:scroll; height:400px; width:1030px; margin-bottom:5px;" onscroll="setScrollPosition(this.scrollTop,'#<%=dv_overView_scoll.ClientID %>');">
                        <asp:GridView ID="GridView_PermitNos" ClientIDMode="Static" runat="server" AutoGenerateColumns="False" DataKeyNames="EMPLID" Width="1010px" HeaderStyle-HorizontalAlign="Left" Font-Size="15px"
                       ShowHeader ="False" class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-medium" HeaderStyle-CssClass="w3-blue" DataSourceID="SqlDataSource_BookPermits" AllowSorting="True" >
                    <Columns>
                    <asp:TemplateField>
                        <ItemTemplate >
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/imgs/show.png" Height="30px" Width="40px" CommandName="Select" data-toggle="modal" data-target="#myModal" />
                        </ItemTemplate>
                        <HeaderStyle Width="60px" />
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>

    
                    <asp:BoundField DataField="PermitNo" HeaderText="PermitNo" SortExpression="PermitNo" ><HeaderStyle Width="140px" /><ItemStyle Width="140px" /></asp:BoundField>
                    <asp:BoundField DataField="FlockNo" HeaderText="FlockNo" SortExpression="FlockNo" ><HeaderStyle Width="110px" /><ItemStyle Width="110px" /></asp:BoundField>
                    <asp:BoundField DataField="NAME" HeaderText="Supp Name" SortExpression="NAME" ><HeaderStyle Width="280px" /><ItemStyle Width="280px" /></asp:BoundField>
                    <asp:CheckBoxField DataField="QA" HeaderText="QA" SortExpression="QA" ><HeaderStyle Width="50px" /><ItemStyle Width="50px" /></asp:CheckBoxField>
                    <%--<asp:BoundField DataField="CertificateNo" HeaderText="Cert No" SortExpression="CertificateNo" ><HeaderStyle Width="100px" /><ItemStyle Width="100px" /></asp:BoundField>--%>
                    <asp:BoundField DataField="BatchDesc" HeaderText="Desc" SortExpression="BatchDesc" ><HeaderStyle Width="80px" /><ItemStyle Width="80px" /></asp:BoundField>
                    <asp:CheckBoxField DataField="PermitRegistered" HeaderText="Reg" SortExpression="PermitRegistered" ><HeaderStyle Width="50px" /><ItemStyle Width="50px" /></asp:CheckBoxField>
                    <asp:CheckBoxField DataField="voauthorisation" HeaderText="VO" SortExpression="voauthorisation" ><HeaderStyle Width="50px" /><ItemStyle Width="50px" /></asp:CheckBoxField>
                    <asp:BoundField DataField="sumNewQty" HeaderText="Qty" ReadOnly="True" SortExpression="sumNewQty" ><HeaderStyle Width="50px" /><ItemStyle Width="50px" /></asp:BoundField>
                         <asp:CheckBoxField DataField="MS" HeaderText="FSF" ReadOnly="True" SortExpression="MS" ><HeaderStyle Width="50px" /><ItemStyle Width="50px" /></asp:CheckBoxField>
                </Columns>

                            <EditRowStyle BackColor="#FFCC00" BorderStyle="None" />

            <HeaderStyle CssClass="w3-blue"></HeaderStyle>
                            
                     <SelectedRowStyle BackColor="#33CCFF" />
            </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_BookPermits" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="
                            SELECT        EMPLH.EMPLID, EMPLH.PermitNo, EMPLH.FlockNo, SuppLier.NAME, EMPLH.QA, EMPLH.CertificateNo, EMPL_Batch_Type.BatchDesc, EMPLH.PermitRegistered, EMPLH.VOAuthorisation, 
                          
						 isnull( sum(EMPL_Line.NewQty-EMPL_Line.DeadOnArrival-EMPL_Line.DeadInPen),0) AS sumNewQty,
								  EMPL_Line.MS
FROM            EMPL_Header AS EMPLH LEFT OUTER JOIN
                         SuppLier ON EMPLH.Suppno = SuppLier.Supplier INNER JOIN
                         EMPL_Batch_Type ON EMPLH.BatchType = EMPL_Batch_Type.BatchType INNER JOIN
                         EMPL_Line ON EMPL_Line.EMPLID = EMPLH.EMPLID
WHERE        (CONVERT(varchar(10), EMPLH.KillDate, 103) = @KillDate)
group by EMPLH.EMPLID, EMPLH.PermitNo, EMPLH.FlockNo, SuppLier.NAME, EMPLH.QA, EMPLH.CertificateNo, EMPL_Batch_Type.BatchDesc, EMPLH.PermitRegistered, EMPLH.VOAuthorisation, EMPL_Line.MS
ORDER BY EMPLH.PermitNo
                            ">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox_Date" Name="KillDate" PropertyName="Text" />
                </SelectParameters>
            </asp:SqlDataSource>
                    </div>
                        <div style="width:1010px; text-align:center; font-size:12px">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:120px"></th>
                                    <th style="width:120px;text-align:center;">LAMBS</th>
                                    <th style="width:120px;text-align:center;">ORGANIC</th>
                                    <th style="width:120px;text-align:center;">RISS(MS)</th>
                                    <th style="width:120px;text-align:center;">HOGGET</th>
                                    <th style="width:120px;text-align:center;">EWE</th>
                                    <th style="width:120px;text-align:center;">RAM</th>
                                    <th style="width:120px;text-align:center;">TOTAL</th>
                                </tr>
                                <tr class="success">
                                    <th>LAIRAGE</th>
                                    <td>
                                        <asp:Label ID="lb_LambLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_OrganicLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_RissLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_HoggetLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_EweLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_RamLair" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_TotalLair" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr class="info">
                                    <th>ABATTOIR</th>
                                    <td>
                                        <asp:Label ID="lb_LambAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_OrganicAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_RissAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_HoggetAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_EweAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_RamAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lb_TotalAbb" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                </div>

        </ContentTemplate>
    </asp:UpdatePanel>


<!-- Modal Begin      -->
  <div  class="modal fade"  id="myModal" role="dialog"  >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Permit Details </h4>            
        </div>
        <div class="modal-body" style="padding-top:0px; padding-bottom:2px;">     
              <asp:Label ID="Label_EMPLID" runat="server" Text="" style="display:none"/>
              <asp:Label ID="Label_boolVOAuthorisation" runat="server" Text="" style="display:none"/>
              <asp:Label ID="Label_boolPermitRegistered" runat="server" Text="" style="display:none"/>
              <asp:Label ID="Label_intBatchType" runat="server" Text="" style="display:none"/>
              <asp:Label ID="Label_strBatchOrTag" runat="server" Text="" style="display:none"/>
            
              <div class="row">
                    <div class="col-md-8">
                            <div id="PermitandBatch" class="informationPanel" style="display:block;">
                             <asp:UpdatePanel ID="UpdatePanel_Modal_PermitAndBatch" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                 <table class="table">
                                   <tr>
                                       <th style="vertical-align:middle">PermitNo:</th>
                                       <td><asp:TextBox ID="TextBox_Modal_PermitNo" class="KeyPadinputSting form-control" runat="server" onClick="setFocusID(this.id)" /></td>
                                       <td>
                                           <table>
                                               <tr>
                                                   <td><asp:Button ID="Button_Modal_Flock" runat="server" Text="Flock" cssclass="btn btn-primary"  Width="75px" OnClientClick ="openInformationPanel(event,'SupplierSearch');return false;" /></td>
                                                   <td><asp:TextBox ID="TextBox_Modal_Flock" runat="server" class="KeyPadinputSting form-control" Width="100px" onClick="setFocusID(this.id)" /></td>
                                                   <td><asp:Button ID="Button_Modal_Quick" runat="server" Text="Search" cssclass="btn btn-primary"  Width="75px"  /></td>
                                               </tr>
                                           </table>
                                       </td>
                                   </tr>
                                   <tr>
                                       <th style="vertical-align:middle">Supplier</th>
                                       <td><asp:Label ID="Label_Modal_SupplierName" runat="server" Text="" /><asp:Label ID="Label_Modal_SupplierNo" runat="server" Text="" Visible ="false"  />
                                       <%--    <asp:DropDownList ID="DropDownList_Modal_Company" cssclass="form-control" runat="server"  DataSourceID="SqlDataSource_Company" DataTextField="DescText" DataValueField="DESTNo"></asp:DropDownList>
                                           <asp:SqlDataSource ID="SqlDataSource_Company" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT DISTINCT [DESTNo], [DescText] FROM [Destination] ORDER BY [DescText]"></asp:SqlDataSource>--%>
                                       </td>
                                       <td rowspan="2">
                                           <asp:Label ID="Label_Modal_Address1" runat="server" Text=""></asp:Label>
                                           <br />
                                           <asp:Label ID="Label_Modal_Address2" runat="server" Text=""></asp:Label>
                                           <br />
                                            <asp:Label ID="Label_Modal_Address3" runat="server" Text=""></asp:Label>
                                       </td>
                                   </tr>
                                   <tr>
                                       <th style="vertical-align:middle">QA:</th>
                                        <td>
                                            <asp:CheckBox ID="CheckBox_Modal_QA"  Class="w3-check" runat="server" />
                                       </td>
                                   </tr>
                                   <tr>
                                       <th style="vertical-align:middle">Email:</th>
                                       <td colspan="2">
                                            <div class="form-inline">
                                           <asp:TextBox ID="TextBox_Modal_Email" class="KeyPadinputSting form-control" Width="350px" runat="server" onClick="setFocusID(this.id)"></asp:TextBox>
                                           <asp:Button ID="Button_Modal_Edit" CssClass="btn btn-primary" runat="server" Text="Edit" /></div>

                                       </td>
                                   </tr>
                              <%--   <tr>
                                     <th style="vertical-align:middle">Certificate:</th>
                                     <td>
                                         <asp:TextBox ID="TextBox_Modal_Certificate" class="KeyPadinputSting form-control" runat="server" onClick="setFocusID(this.id)" /></td>
                                     <td>
                                         <table>
                                             <tr>
                                                 <th style="vertical-align:middle">Port:</th>
                                                 <td>
                                                     <asp:DropDownList ID="DropDownList_Modal_Port" cssclass="form-control" runat="server" DataSourceID="SqlDataSource_Modal_PortImport" DataTextField="PortDesc" DataValueField="PortID"></asp:DropDownList>
                                                     <asp:SqlDataSource ID="SqlDataSource_Modal_PortImport" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT PortID, PortDesc FROM EMPL_Port_Import"></asp:SqlDataSource>
                                                     <asp:TextBox ID="TextBox_Modal_Port" class="KeyPadinputSting form-control" runat="server"></asp:TextBox></td>
                                             </tr>
                                         </table>
                                     </td>
                                 </tr>--%>
                               </table>
           <%--                       <div>Permit Batches</div>
                                  <table  class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small" style="Width:550px">
                                       <tr>
                                           <th style="width:180px;">Sex</th>
                                           <th style="width:180px;">Qty</th>
                                           <th style="width:190px;">Dest</th>
                                       </tr>
                                  </table>--%>
                                  <asp:Label ID="Label_BatchCount" runat="server" Text="" style="display:none" />
                                
                                  <div style="height:200px;overflow-y:scroll; background-color:lightyellow; margin-bottom:10px;">
                                      <asp:GridView ID="GridView_PermitBatches" runat="server" DataKeyNames="EMPLID,EMPLLineID,Company" Width="550px" AutoGenerateColumns="False"
                                          RowStyle-VerticalAlign="Middle" RowStyle-Height="40px" ShowHeaderWhenEmpty="True" Caption="Permit Batches" 
                                          CssClass="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small" DataSourceID="SqlDataSource_PermitBatches">
                                          <Columns>
                                             
                                              <asp:BoundField DataField="specie" HeaderText="Sex" SortExpression="specie" >
                                               <HeaderStyle Width="180px" />
                                              </asp:BoundField>
                                               <asp:BoundField DataField="NewQty" HeaderText="Qty" SortExpression="NewQty" >
                                               <HeaderStyle Width="180px" />
                                              </asp:BoundField>
                                               <asp:BoundField DataField="dest" HeaderText="Dest" SortExpression="dest" >
                                              <HeaderStyle Width="190px" />
                                              </asp:BoundField>
                                          </Columns>
                                          <RowStyle Height="40px" VerticalAlign="Middle" />
                                          <SelectedRowStyle BackColor="#33CCFF" />
                                      </asp:GridView>
                                       <%--<asp:GridView ID="GridView_PermitBatches" runat="server" AutoGenerateColumns="False" Width="550px"
                                           class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small" Font-Size="11px"  RowStyle-VerticalAlign="Middle"                                         
                                           DataKeyNames="EMPLID,EMPLLineID" Caption="Permit Batches" DataSourceID="SqlDataSource_PermitBatches">
                                           <Columns>
                                               <asp:BoundField DataField="specie" HeaderText="Sex" SortExpression="specie" />
                                               <asp:BoundField DataField="NewQty" HeaderText="Qty" SortExpression="NewQty" />
                                           <asp:BoundField DataField="tagFrom" HeaderText="Frm Tag" SortExpression="tagFrom" />
                                               <asp:BoundField DataField="tagto" HeaderText="To Tag" SortExpression="tagto" />
                                               <asp:BoundField DataField="tagColour" HeaderText="tagColour" SortExpression="tagColour" />
                                               <asp:BoundField DataField="EMPLGroup" HeaderText="EMPLGroup" SortExpression="EMPLGroup" />
                                               <asp:BoundField DataField="dest" HeaderText="Dest" SortExpression="dest" />
                                           </Columns>
                                       </asp:GridView>--%>
                                       <asp:SqlDataSource ID="SqlDataSource_PermitBatches" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT EMPLID,EMPLLineID,specie,qty,NewQty,tagFrom,tagto,tagColour,EMPLGroup,batchno,martlotno,movetoabb,Company,permitRegistered, QA, destination.description as dest FROM EMPL_Line 
                                            INNER JOIN sex on sex.ec_code = EMPL_line.specie INNER JOIN destination ON destination.destno = EMPL_Line.company WHERE  EMPLID =@EMPLID  AND moveToAbb =0" DeleteCommand="DELETE FROM EMPL_line WHERE EMPLID = @EMPLID AND EMPLLineID =@EMPLLineID">
                                           <DeleteParameters>
                                               <asp:Parameter Name="EMPLID" />
                                               <asp:Parameter Name="EMPLLineID" />
                                           </DeleteParameters>
                                           <SelectParameters>
                                               <asp:ControlParameter ControlID="Label_EMPLID" Name="EMPLID" PropertyName="Text" />
                                           </SelectParameters>
                                       </asp:SqlDataSource>
                                   </div>
                                  <table class="w3-table" style="text-align:center;">
                                            <tr>
                                                <td><asp:Button ID="Button_Modal_Update" CssClass="btn btn-info" Font-Size="Large" runat="server" Text="Update"  Width="100px" Height="50" CausesValidation="true" /></td>
                                                <td><asp:Button ID="Button_Modal_Add" CssClass="btn btn-primary" runat="server" Text="Add Batch"  Width="100px"  Height="35" data-toggle="modal" data-target="#myModal_NewBatch" OnClientClick="HideModal('#myModal');" /></td>
                                                <td><asp:Button ID="Button_Modal_Delete" CssClass="btn btn-primary" runat="server" Text="Delete"  Width="100px" Height="35" />
                                                    <ajaxToolkit:ConfirmButtonExtender ID="Button_Modal_Delete_ConfirmButtonExtender" runat="server" BehaviorID="Button_Modal_Delete_ConfirmButtonExtender" ConfirmText="Are you sure you want to delete Selected Batch?" TargetControlID="Button_Modal_Delete" />
                                                </td>
                                                <td><asp:Button ID="Button_Modal_ChangeDest" CssClass="btn btn-primary" runat="server" Text="Change"  Width="100px"  Height="35" data-toggle="modal" data-target="#myModal_NewBatch"  OnClientClick="HideModal('#myModal');" /></td>
                                                <td><asp:Button ID="Button_Modal_Back" class="btn btn-default" data-dismiss="modal" Font-Size="Large" runat="server" Text="Close"  Width="100px" Height="50" /></td>
                                            </tr>
                                     </table>
                                 </ContentTemplate>
                             </asp:UpdatePanel>
                        </div>
                            <div id="SupplierSearch" class="informationPanel" style="display:none;"> 
                                <asp:UpdatePanel ID="UpdatePanel_informationPanel_Supllier" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <table style="margin-top:5px">
                                            <tr>
                                                <td>Supplier:&nbsp;</td>
                                                <td><asp:TextBox ID="TextBox_Modal_SupplierKeyword" runat="server" class="KeyPadinputSting form-control" onClick="setFocusID(this.id)"  OnTextChanged="TextBox_Modal_SupplierKeyword_TextChanged" AutoPostBack="True" />
                                                    <ajaxToolkit:AutoCompleteExtender ID="TextBox_Modal_SupplierKeyword_AutoCompleteExtender" runat="server" BehaviorID="TextBox_Modal_SupplierKeyword_AutoCompleteExtender" DelimiterCharacters="" 
                                                        ServiceMethod="SearchCustomers"  MinimumPrefixLength="2" TargetControlID="TextBox_Modal_SupplierKeyword" CompletionInterval="100" EnableCaching="false" CompletionSetCount="10">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </td>
                                                <td rowspan="2">
                                                   <%--  <input id="Button_Supplier_Up" type="button" value="UP" class="btn btn-info" style="width:70px" onclick="ScollUp('GridviewDiv',45)"  />--%>
                                                    <%-- <input id="Button_Supplier_Up" type="button" value="UP" class="btn btn-info" style="width:80px" onclick="ScollUpModalSupplier('Gridview_SupplierDiv', 45)"  />--%>
                                                    <asp:Button ID="Button10" runat="server" Text="Close" cssclass="btn btn-primary" style="margin-left:10px"  Width="75px" OnClientClick ="openInformationPanel(event,'PermitandBatch');return false;" />
                                                    <%--<asp:Button ID="Button17" runat="server" Text="Up" CssClass="btn btn-info" Font-Size="Large" Width="80px" OnClientClick="supplierModalScoll('Gridview_SupplierDiv',45,'Up'); return false;" />
                                                    <asp:Button ID="Button18" runat="server" Text="Down" CssClass="btn btn-info" Font-Size="Large" Width="80px" onClientclick="supplierModalScoll('Gridview_SupplierDiv',45,'Down'); return false;" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="HiddenField_Modal_Supplier" Value="0" runat="server" />
                                        <br />
                                        <div id="Gridview_SupplierDiv" style="height:250px; overflow-y:scroll"  onscroll="setSupplierScrollPosition(this.scrollTop,'#<%=HiddenField_Modal_Supplier.ClientID %>');">                                        
                                            <asp:GridView ID="GridView_Modal_Suppliers" runat="server" AutoGenerateColumns="False" DataKeyNames="Supplier" RowStyle-Height="40px" 
                                                DataSourceID="SqlDataSource_Suppliers" class="w3-table w3-striped w3-bordered w3-hoverable w3-card-4 w3-small" Font-Size="11px"  RowStyle-VerticalAlign="Middle" >
                                                <AlternatingRowStyle VerticalAlign="Middle"  />
                                                <Columns >
                                                    <%--<asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton_SupplierName" Text='<%# Eval("NAME") %>' runat="server" ></asp:LinkButton>                                                  
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="NAME" HeaderText="NAME" SortExpression="NAME" ItemStyle-Height="45px" >
                                                    <ItemStyle Height="45px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="HerdNo" HeaderText="HerdNo" SortExpression="HerdNo" />
                                                    <asp:BoundField DataField="ADDRESS1" HeaderText="ADDRESS1" SortExpression="ADDRESS1" />
                                                    <asp:BoundField DataField="ADDRESS2" HeaderText="ADDRESS2" SortExpression="ADDRESS2" />
                                                </Columns>
                                                <SelectedRowStyle BackColor="#33CCFF" />
                                            </asp:GridView>
                                        </div>
                                        <asp:SqlDataSource ID="SqlDataSource_Suppliers" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT [Supplier], [NAME], [ADDRESS1], [ADDRESS2], [HerdNo], [EmailAddress] FROM [SuppLier] WHERE (([HerdNo] LIKE '%' + @Keyword + '%') OR ([NAME] LIKE '%' + @Keyword + '%'))">
                                            <SelectParameters>                                   
                                                <asp:ControlParameter ControlID="TextBox_Modal_SupplierKeyword" Name="Keyword" PropertyName="Text" Type="String" DefaultValue="" />                                       
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                            
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>           
                    </div>
                    <div class="col-md-4">
                          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                           <ContentTemplate>

                            <div class="w3-row">
                              <a href="#" onclick="openKeyBoard(event, 'NUMBER');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red">NUMBER</div>
                              </a>
                              <a href="#" onclick="openKeyBoard(event, 'ALPHABET');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding">ALPHABET</div>
                              </a>
                          
                            </div>

                            <div id="NUMBER" class="w3-container KeyBoard" style="display:block; height:360px">
                                <div class="row" style="margin-top:10px">
                                    <div class="col-sm-4"><asp:Button ID="Button7" runat="server" Text="7" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button8" runat="server" Text="8" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button9" runat="server" Text="9" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button4" runat="server" Text="4" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button5" runat="server" Text="5" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button6" runat="server" Text="6" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button1" runat="server" Text="1" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button2" runat="server" Text="2" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="auto-style1"><asp:Button ID="Button3" runat="server" Text="3" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button11" runat="server" Text="0" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button12" runat="server" Text="F" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button13" runat="server" Text="DEL" class="btn btn-info KeyBoardStyle" OnClientClick="ketPayDelete();return false;" /></div>
                                </div>
                                   <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button14" runat="server" Text="." class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button15" runat="server" Text="-" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button16" runat="server" Text="@" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-sm-12"><asp:Button ID="Button_Enter" ClientIDMode="Static" runat="server" Text="ENTER" class="btn btn-info KeyBoardStyle" Width="245px" /></div>                                  
                                </div>
                            </div>

                            <div id="ALPHABET" class="w3-container KeyBoard" style="height:360px">
                                <div class="row" style="margin-bottom:5px; margin-top:10px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button19" runat="server" Text="A" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button21" runat="server" Text="B" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button22" runat="server" Text="C" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button23" runat="server" Text="D" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>      
                                 
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button24" runat="server" Text="E" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button25" runat="server" Text="F" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button26" runat="server" Text="G" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button27" runat="server" Text="H" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>           
                                        
                                 
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button28" runat="server" Text="I" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button29" runat="server" Text="J" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button30" runat="server" Text="K" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button31" runat="server" Text="L" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>    
                                       
                                    
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button32" runat="server" Text="M" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button33" runat="server" Text="N" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button34" runat="server" Text="O" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button35" runat="server" Text="P" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div> 
                                     
                                   
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                  
                                    <div class="col-sm-3"><asp:Button ID="Button36" runat="server" Text="Q" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button37" runat="server" Text="R" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button38" runat="server" Text="S" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button39" runat="server" Text="T" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>

                                    
                                </div>
                                <div class="row" style="margin-bottom:6px">
                                   
                                    <div class="col-sm-3"><asp:Button ID="Button40" runat="server" Text="U" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button41" runat="server" Text="V" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button42" runat="server" Text="W" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button43" runat="server" Text="X" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>

                                </div>
                                <div class="row" style="margin-bottom:10px">     
                                    <div class="col-sm-3"><asp:Button ID="Button44" runat="server" Text="Y" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button45" runat="server" Text="Z" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button46" runat="server" Text=" " class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button47" runat="server" Text="Del" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="ketPayDelete();" /></div>
                                </div>

                            </div>
                            <table class="w3-table" style="text-align:center; margin-top:5px">
                                <tr>
                                    <td><asp:Button ID="Button_Modal_Farm" runat="server" Text="Farm" CssClass="btn btn-success"  Width="120px" Height="50px" /></td>
                                    <td><asp:Button ID="Button_Modal_Mart" runat="server" Text="Mart" CssClass="btn btn-danger"  Width="120px" Height="50px" /></td>
                                </tr>
                              <%--   <tr>
                                    <td><asp:Button ID="Button_Modal_DirectImport" runat="server" Text="Direct Import" CssClass="btn btn-danger"  Width="120px" Height="50px" /></td>
                                    <td><asp:Button ID="Button_Modal_PortImport" runat="server" Text="Port Import" CssClass="btn btn-danger"  Width="120px" Height="50px" /></td>
                                </tr>--%>
                            </table>      
                               
                           </ContentTemplate>
                       </asp:UpdatePanel>           
                    </div>
              </div>
                     <table>
               <tr>
                   <!-- left -->
                   <td style="vertical-align:top;">                                             
                    
                   </td>
                   <!-- Right -->
                   <td style="width:275px; vertical-align:top;">
                           
                   </td>
               </tr>
           </table>
                         <script>
                                    function openKeyBoard(evt, Name) {
                                      var i, x, tablinks;
                                      x = document.getElementsByClassName("KeyBoard");
                                      for (i = 0; i < x.length; i++) {
                                         x[i].style.display = "none";
                                      }
                                      tablinks = document.getElementsByClassName("tablink");
                                      for (i = 0; i < x.length; i++) {
                                         tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
                                      }
                                      document.getElementById(Name).style.display = "block";
                                      evt.currentTarget.firstElementChild.className += " w3-border-red";
                                    }

                                    function openInformationPanel(evt,panel ) {
                                        var i, x, tablinks;
                                        x = document.getElementsByClassName("informationPanel");
                                        for (i = 0; i < x.length; i++) {
                                            x[i].style.display = "none";
                                        }
                                        //tablinks = document.getElementsByClassName("tablink");
                                        //for (i = 0; i < x.length; i++) {
                                        //    tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
                                        //}
                                        document.getElementById(panel).style.display = "block";
                                        //evt.currentTarget.firstElementChild.className += " w3-border-red";
                                    }
                                   
                                    //function btnKeypad_click() {
                                    //    var $focused = $(':focus');
                                    //    alert($focused);
                                    //    return false;

                                    //}
                             </script>

           
          
        </div>
        <div class="modal-footer" style="padding-top:5px;">
            <div id="modal_Error_alert" class="alert alert-danger"; style="padding-top:10px; padding-bottom:10px; margin-bottom:5px;">
                <a href="#" class="close" aria-label="close">&times;</a>
                <strong>Warning!</strong> <asp:Label ID="Label_Modal_AlertMessage" ClientIDMode="Static" runat="server" Text=""></asp:Label>
            </div>
            <div id="modal_Confirm_alert" class="alert alert-success"; style="padding-top:10px; padding-bottom:10px; margin-bottom:5px;">
                <a href="#" class="close" aria-label="close">&times;</a>
                <strong></strong> <asp:Label ID="Label_Modal_AlertSuccess" ClientIDMode="Static" runat="server" Text=""></asp:Label>
            </div>  
        </div>
      </div>
    </div>
  </div>
<!-- Modal End -->

    <!--class="modal fade"    -->
    <div class="modal fade" id="myModal_NewBatch" role="dialog" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pick Specie </h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_PickSpecie" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="Label_Modal_PickSpecie_New_Update" runat="server" Text="" Visible ="false" ></asp:Label>
                    <div class="form-inline form-group">Destination: <asp:DropDownList ID="DropDownList_Model_PickSpecie_Destination" cssclass="form-control" style="height:auto; font-size:20px;" runat="server" DataSourceID="SqlDataSource_Modal_Destination" DataTextField="DescText" DataValueField="DESTNo" CausesValidation="true" onchange="PickSpecieValidation()"  ></asp:DropDownList>
                       <asp:SqlDataSource ID="SqlDataSource_Modal_Destination" runat="server" ConnectionString="<%$ ConnectionStrings:AMPSLindenLambConnectionString %>" SelectCommand="SELECT DESTNo, DescText FROM Destination ORDER BY DescText"></asp:SqlDataSource>
                        <div id="PickSpecie_modal_Destination_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; display:none">
                            <strong>Warning!</strong> Blank is not allowed in Destination
                        </div>
                    </div>
                    <div>
                           <ul class="w3-ul w3-large"  style="margin-top:10px; margin-bottom:10px" >
                            <li class="w3-hover-red"><asp:CheckBox ID="CheckBox_12months" runat="server" Text=" The animals are less than 12 months old?" onchange="PickSpecieValidation()"  />
                                <div id="PickSpecie_modal_CheckBox12months_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                            <li class="w3-hover-green"><asp:CheckBox ID="CheckBox_30days" runat="server" Text=" The animals have resided on the last farm 60 days?"  onchange="PickSpecieValidation()"  />
                                 <div id="PickSpecie_modal_CheckBox30days_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                            <li class="w3-hover-orange"><asp:CheckBox ID="CheckBox_3farm" runat="server" Text=" The animals have been on 3 farms or less?"  onchange="PickSpecieValidation()"  />
                                 <div id="PickSpecie_modal_CheckBox3farm_alert" class="alert alert-danger" style="padding-top:3px; padding-bottom:3px; margin-bottom:3px; float:right; font-size:15px;display:none">
                                    <strong>Warning!</strong> Required
                                </div>
                            </li>
                          </ul>
                    </div>
                      <div class="row" style="margin-bottom:10px">
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Lamb" CssClass="btn btn-info btn-lg" runat="server" Text="Lamb" Width="200px" Height="60px" CausesValidation="true" ValidationGroup="PickSpecie" OnClientClick="return PickSpecieValidation();" /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_OrganicLamb" CssClass="btn btn-info btn-lg" runat="server" Text="Organic Lamb" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_RissLamb" CssClass="btn btn-info btn-lg" runat="server" Text="Rissington Lamb" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                       </div>
                        <div class="row">
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Hogget" CssClass="btn btn-info btn-lg" runat="server" Text="Hogget" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Ram" CssClass="btn btn-info btn-lg" runat="server" Text="Ram" Width="200px" Height="60px"  CausesValidation="true" /></div>
                           <div class="col-md-4"><asp:Button ID="Button_Modal_PickSpecie_Eve" CssClass="btn btn-info btn-lg" runat="server" Text="Eve" Width="200px" Height="60px"  CausesValidation="true"  /></div>
                       </div>
          
                     
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" Runat="Server">
</asp:Content>

