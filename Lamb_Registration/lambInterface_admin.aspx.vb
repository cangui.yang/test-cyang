﻿Imports System.Data.SqlClient

Partial Class lambInterface_admin
    Inherits System.Web.UI.Page

    Dim runQuery As New SQLAccess
    Protected Sub Button_ReturnLastLotToLairage_Click(sender As Object, e As EventArgs) Handles Button_ReturnLastLotToLairage.Click

        'Dim strSQL As String
        'Dim strDateMin As String
        'Dim strDateMax As String
        Dim intBatchno As Integer
        Dim intLotNo As Integer
        'Dim intLairageBatchno As Integer
        'Dim intChoice As Integer
        'Dim intCountOfRecords As Integer
        'Dim intResultChangeBatch As Integer
        'Dim intResultMoveToAbb As Integer
        Dim intEMPLID As Integer
        Dim intEMPLLineID As Integer


        Try
            intLotNo = runQuery.getScalarInt("SELECT MAX(lotno) FROM EMPL_Line WHERE convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)")
            intEMPLID = runQuery.getScalarInt("SELECT EMPLID FROM EMPL_Line WHERE lotno = " & intLotNo & " AND convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)")
            intEMPLLineID = runQuery.getScalarInt("SELECT EMPLlineID FROM EMPL_Line WHERE lotno = " & intLotNo & " AND convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)")
            intBatchno = runQuery.getScalarInt("SELECT batchno FROM EMPL_Line WHERE lotno = " & intLotNo & " AND convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)")
            Session("LotNo") = intLotNo
            Session("EMPLID") = intEMPLID
            Session("EMPLLineID") = intEMPLLineID
            Session("Batchno") = intBatchno
            If intBatchno = 0 Then
                ShowAlertMessage("There have been no batches moved to the Abattoir yet", "alert alert-warning")
            Else
                ShowAlertMessage("Do you want to return Lot " & intLotNo & " to the lairage?", "alert alert-info", "Return Last Lot to Lairage", True)
            End If

        Catch objError As System.Data.SqlClient.SqlException
            ShowAlertMessage(objError.Message, "alert alert-danger")
        Catch
            ShowAlertMessage(Err.Description, "alert alert-danger")
        End Try
    End Sub
    Sub ReturnLastLotToLairage(ByVal intLotNo As Integer, ByVal intBatchno As Integer, ByVal intEMPLID As Integer, ByVal intEMPLLineID As Integer)

        Dim intResultMoveToAbb As Integer
        'Comment out to verify that no animals have been killed

        intResultMoveToAbb = returnToLairageLive(intBatchno)
        If intResultMoveToAbb = 0 Then
            ShowAlertMessage("Return to Lairage Failed. Change Batch Failed.", "alert alert-warning")
            Exit Sub
        End If

        'Updates the EMPL_Line that the batch is now back in the lairage

        runQuery.executeQuery("UPDATE EMPL_Line SET Lotno = 0, MoveToAbb =0 , BATCHTime = NULL, AssignStatus = 0,slaughterqty = 0 WHERE EMPLID = " & intEMPLID & " AND EMPLLineID = " & intEMPLLineID, "AMPSLindenLambConnectionString")
        runQuery.executeQuery("INSERT INTO EMPL_Return_Log (EMPLID, EMPLLineID, Lotno) VALUES (" & intEMPLID & ", " & intEMPLLineID & ", " & intLotNo & ")", "AMPSLindenLambConnectionString")
        ShowAlertMessage("Return to Lairage Successfull", "alert alert-success")


    End Sub
    Sub ChangeQtyinAnAbbatoir(ByVal batchno As Integer)
        Dim intResultReturnToLairage As Integer = returnToLairageLive(batchno)
        Dim intResultChangedQty As Integer = 0
        Dim intResultMoveBatchToAbb As Integer = 0
        Dim LotNo As String = Label_LotNo_ChangeQtyInAbbatoir.Text
        If intResultReturnToLairage = 0 Then
            ShowAlertMessage("part 1: Return to Lairage failed, please try again.", "alert alert-danger")
            Exit Sub
        Else
            intResultChangedQty = changeQtyInBatchLive(batchno)
            If intResultChangedQty = 0 Then
                runQuery.executeQuery("UPDATE EMPL_Line SET AssignStatus =0 WHERE Lotno =" & LotNo & " and convert(varchar(10),Batchtime,120)=convert(varchar(10),getdate(),120)", "AMPSLindenLambConnectionString")
                ShowAlertMessage("part 2: Change Qty failed. The batch is now in the Lairage. Use 'Move to Abattoir' to change qty and return it.", "alert alert-danger", "AMPSLindenLambConnectionString")
                Exit Sub
            Else
                runQuery.executeQuery("UPDATE EMPL_Line SET newqty = " & TextBox_NewQty__ChangeQtyInAbbatoir.Text & " where Lotno=" & LotNo & " and convert(varchar(10),Batchtime,120)=convert(varchar(10),getdate(),120)", "AMPSLindenLambConnectionString")
                intResultMoveBatchToAbb = MoveBatchToAbb(batchno)
                If intResultMoveBatchToAbb = 0 Then
                    runQuery.executeQuery("UPDATE EMPL_Line SET AssignStatus =3 WHERE Lotno = " & LotNo & " and convert(varchar(10),Batchtime,120)=convert(varchar(10),getdate(),120)", "AMPSLindenLambConnectionString")
                    ShowAlertMessage("part 3: Return to Abbatoir failed. The bath is now in the Lairage. Use 'Move to Abbatoir' to return it.", "alert alert-danger")
                    Exit Sub
                Else
                    Dim intLambCountDiff As Integer = CInt(TextBox_NewQty__ChangeQtyInAbbatoir.Text) - CInt(TextBox_Current__ChangeQtyInAbbatoir.Text)
                    runQuery.executeQuery("DELETE FROM killtrans WHERE Lotno >= " & LotNo & " AND convert(varchar(10),transdate,120) =convert(varchar(10),getdate(),120) ", "AMPSLindenLambConnectionString")
                    runQuery.executeQuery("DELETE FROM Payslips WHERE Lotno >= " & LotNo & " AND convert(varchar(10),kill_date,120) =convert(varchar(10),getdate(),120) ", "AMPSLindenLambConnectionString")
                    runQuery.executeQuery("UPDATE Lairage SET accepted = " & TextBox_NewQty__ChangeQtyInAbbatoir.Text & " where covnert(varchar(10),[Date],120)=convert(varchar(10),getdate(),120) and Lotno=" & LotNo, "AMPSLindenLambConnectionString")
                    runQuery.executeQuery("UPDATE Lairage SET firstkillno = firstkillno + " & intLambCountDiff & " WHERE covnert(varchar(10),[Date],120)=convert(varchar(10),getdate(),120) AND Lotno > " & LotNo, "AMPSLindenLambConnectionString")
                    runQuery.executeQuery("UPDATE Lairage SET transfer = 0, LTrans = 0, killed= 0 WHERE covnert(varchar(10),[Date],120)=convert(varchar(10),getdate(),120) AND Lotno >= " & LotNo, "AMPSLindenLambConnectionString")
                    ShowAlertMessage("Change quantity successfull", "alert alert-success")
                End If
            End If

        End If



    End Sub
    Sub ShowAlertMessage(ByVal Msg As String, ByVal messageType As String, Optional ByVal app As String = "", Optional ByVal displayYesNoButton As Boolean = False)
        Label_AlertMessage.Text = Msg
        Button_Alert_Yes.Visible = False
        Button_Alert_No.Visible = False
        Label_Application.Text = app

        If displayYesNoButton Then
            Button_Alert_Yes.Visible = True
            Button_Alert_No.Visible = True
        End If

        UpdatePanel_Alert.Update()
        ScriptManager.RegisterStartupScript(Button_ReturnLastLotToLairage, GetType(Page), "UpdateDiv", "UpdateAlertClass('" & messageType & "');", True)

        ScriptManager.RegisterStartupScript(Button_ReturnLastLotToLairage, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)
    End Sub


    Private Function returnToLairageLive(ByVal intBatchno As Integer) As Integer

        Dim ws_Generic As New WS_SheepReturnToLairage.GenericInput
        Dim ws_ws_RTLairage_InfoInput As New WS_SheepReturnToLairage.SheepReturnToLairageInput
        Dim ws_ws_RTLairage_InfoOutput As New WS_SheepReturnToLairage.SheepReturnToLairageOutput
        Dim ws_RTLairage As New WS_SheepReturnToLairage.WS_SheepReturnToLairagePortTypeClient


        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        'ws_Generic.clntRefNo_BusId = 993336
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_ws_RTLairage_InfoInput.genericInput = ws_Generic
        ws_ws_RTLairage_InfoInput.pinNo = "003"
        ws_ws_RTLairage_InfoInput.batchNo = intBatchno

        ws_ws_RTLairage_InfoOutput = ws_RTLairage.WS_SheepReturnToLairage(ws_ws_RTLairage_InfoInput)

        If ws_ws_RTLairage_InfoOutput.success = 1 Then

            Return ws_ws_RTLairage_InfoOutput.success

        Else
            ShowAlertMessage("Error: " & ws_ws_RTLairage_InfoOutput.error & " ErrorCode: " & ws_ws_RTLairage_InfoOutput.errorCode, "alert alert-danger")
            Return ws_ws_RTLairage_InfoOutput.success
        End If

    End Function
    Private Function changeQtyInBatchLive(ByVal intbatchno As Integer) As Integer
        Dim ws_Generic As New WS_SheepAmendNumberInBatch.GenericInput
        Dim ws_SCQIB_InfoInput As New WS_SheepAmendNumberInBatch.SheepAmendNumberInBatchInput
        Dim ws_SCQIB_InfoOutput As New WS_SheepAmendNumberInBatch.SheepAmendNumberInBatchOutput
        Dim ws_SCQIB As New WS_SheepAmendNumberInBatch.WS_SheepAmendNumberInBatchPortTypeClient

        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        'ws_Generic.clntRefNo_BusId = 993336
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_SCQIB_InfoInput.genericInput = ws_Generic
        ws_SCQIB_InfoInput.pinNo = "003"
        ws_SCQIB_InfoInput.batchNo = intbatchno

        ws_SCQIB_InfoOutput = ws_SCQIB.WS_SheepAmendNumberInBatch(ws_SCQIB_InfoInput)

        If ws_SCQIB_InfoOutput.success = 1 Then

            Return ws_SCQIB_InfoOutput.success

        Else
            ShowAlertMessage("Error: " & ws_SCQIB_InfoOutput.error & " ErrorCode: " & ws_SCQIB_InfoOutput.errorCode, "alert alert-danger")
            Return ws_SCQIB_InfoOutput.success
        End If

    End Function
    Function MoveBatchToAbb(ByVal intBatchno As Integer) As Integer
        Try
            Dim ws_Generic As New WS_SheepMoveToAbatLive.GenericInput
            Dim ws_SMTA_InfoInput As New WS_SheepMoveToAbatLive.SheepMoveToAbatInput
            Dim ws_SMTA_InfoOutput As New WS_SheepMoveToAbatLive.SheepMoveToAbatOutput
            Dim ws_SMTA As New WS_SheepMoveToAbatLive.WS_SheepMoveToAbatPortTypeClient

            ws_Generic.callingLocation = "Meat"
            ws_Generic.channel = "Meat"
            'ws_Generic.clntRefNo_BusId = 993336
            ws_Generic.clntRefNo_BusId = 922225
            ws_Generic.environment = "Live"
            ws_Generic.herdNo = "A04001"
            ws_Generic.password = ""
            ws_Generic.username = "LINS01"

            ws_SMTA_InfoInput.genericInput = ws_Generic
            ws_SMTA_InfoInput.pinNo = "003"

            ws_SMTA_InfoInput.batchNo = intBatchno
            'ws_SMTA_InfoInput.batchNoSpecifie = True
            ws_SMTA_InfoOutput = ws_SMTA.WS_SheepMoveToAbat(ws_SMTA_InfoInput)

            If ws_SMTA_InfoOutput.success = 1 Then
                Return ws_SMTA_InfoOutput.success
            Else
                ShowAlertMessage("Error: " & ws_SMTA_InfoOutput.error & " ErrorCode: " & ws_SMTA_InfoOutput.errorCode, "alert alert-danger")
                Return ws_SMTA_InfoOutput.success
            End If
        Catch ex As Exception
            ShowAlertMessage("Error on MoveBatchToAbbatior: " & ex.Message, "alert alert-danger")
        End Try
    End Function
    Private Function editBatchNoLive(ByVal intOrigBatch As Integer, ByVal intNewBatch As Integer) As Integer

        'Dim ws_EBN As New WS_SheepEditBatchLive.WS_SheepEditBatchNo
        Dim ws_EBN As New WS_SheepEditBatchNo.WS_SheepEditBatchNoPortTypeClient
        Dim ws_Generic As New WS_SheepEditBatchNo.GenericInput
        Dim ws_EBN_InfoInput As New WS_SheepEditBatchNo.SheepEditBatchNoInput
        Dim ws_EBN_InfoOutput As New WS_SheepEditBatchNo.SheepEditBatchNoOutput

        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        'ws_Generic.clntRefNo_BusId = 993336
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_EBN_InfoInput.genericInput = ws_Generic
        ws_EBN_InfoInput.pinNo = "003"

        ws_EBN_InfoInput.newBatchNo = intNewBatch
        ws_EBN_InfoInput.origBatchNo = intOrigBatch

        ws_EBN_InfoOutput = ws_EBN.WS_SheepEditBatchNo(ws_EBN_InfoInput)

        If ws_EBN_InfoOutput.success = 1 Then

            Return ws_EBN_InfoOutput.success

        Else
            ShowAlertMessage("Error: " & ws_EBN_InfoOutput.error & " ErrorCode: " & ws_EBN_InfoOutput.errorCode, "alert alert-danger")
            Return ws_EBN_InfoOutput.success

        End If

        'Return ws_EBN_InfoOutput.success

    End Function
    Protected Sub Button_Alert_Yes_Click(sender As Object, e As EventArgs) Handles Button_Alert_Yes.Click
        Select Case Label_Application.Text
            Case "Return Last Lot to Lairage"
                ReturnLastLotToLairage(Session("LotNo"), Session("Batchno"), Session("EMPLID"), Session("EMPLLineID"))
            Case "ChangeQtyInAbbatoir"
                ChangeQtyinAnAbbatoir(Session("Batchno"))
            Case Else

        End Select
    End Sub

    Protected Sub Button_SubmitBatchno_Modal_ChangeQtyInAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_SubmitBatchno_Modal_ChangeQtyInAbbatoir.Click

        Dim LotNo As Integer = CInt(TextBox_Batchno_Modal_ChangeQtyInAbbatoir.Text)
        Dim intCountRecords As Integer = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_Line WHERE lotno = " & LotNo & " and convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)")
        If intCountRecords = 1 Then
            Label_LotNo_ChangeQtyInAbbatoir.Text = TextBox_Batchno_Modal_ChangeQtyInAbbatoir.Text

            Dim strSQL As String
            Dim daConnect As SqlDataAdapter
            Dim dsBatchno As New Data.DataSet

            strSQL = "SELECT qty,newqty FROM EMPL_line WHERE lotno = " & LotNo & " AND convert(varchar(10),batchTime,120)= convert(varchar(10),GetDate(),120)"


            daConnect = New SqlDataAdapter(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
            daConnect.Fill(dsBatchno, "BatchQuantities")
            daConnect.Dispose()

            If dsBatchno.Tables(0).Rows.Count = 1 Then
                TextBox_Original_ChangeQtyInAbbatoir.Text = dsBatchno.Tables(0).Rows(0).Item("qty")
                TextBox_Current__ChangeQtyInAbbatoir.Text = dsBatchno.Tables(0).Rows(0).Item("newqty")
                TextBox_NewQty__ChangeQtyInAbbatoir.Text = dsBatchno.Tables(0).Rows(0).Item("newqty")
            Else
                Label_Alert_ChangeQtyInAbbatoir.Text = "<div Class=""alert alert-warning""><strong>Warning! </strong> Error returning batch quantities. Change cancelled </div>"
            End If

            ScriptManager.RegisterStartupScript(Button_ChangeQtyInAnAbbatoir, GetType(Page), "ShowInformationPanel", "openInformationPanel('informationPanel','ChangeAbbQty_ChangeQtyInAbbatoir');", True)
        Else

            Label_Alert_ChangeQtyInAbbatoir.Text = "<div Class=""alert alert-warning""><strong>Warning!</strong> Lotno " & TextBox_Batchno_Modal_ChangeQtyInAbbatoir.Text & " not found. Change cancelled </div>"

        End If

    End Sub
    Protected Sub Button_Plus_ChangeQtyInAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_Plus_ChangeQtyInAbbatoir.Click
        TextBox_NewQty__ChangeQtyInAbbatoir.Text = CInt(TextBox_NewQty__ChangeQtyInAbbatoir.Text) + 1
    End Sub
    Protected Sub Button_Reduce_ChangeQtyInAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_Reduce_ChangeQtyInAbbatoir.Click
        If TextBox_NewQty__ChangeQtyInAbbatoir.Text > 0 Then
            TextBox_NewQty__ChangeQtyInAbbatoir.Text = CInt(TextBox_NewQty__ChangeQtyInAbbatoir.Text) - 1
        End If
    End Sub
    Protected Sub Button_Update_ChangeQtyInAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_Update_ChangeQtyInAbbatoir.Click
        Dim boolChangeQty As Boolean = True
        Dim intCountOfAnimals As Integer

        If TextBox_NewQty__ChangeQtyInAbbatoir.Text = TextBox_Current__ChangeQtyInAbbatoir.Text Or TextBox_NewQty__ChangeQtyInAbbatoir.Text < 1 Then
            ShowAlertMessage("No Change made", "alert alert-warning")
            boolChangeQty = False
        End If
        intCountOfAnimals = runQuery.getScalarInt("SELECT COUNT([kill]) FROM tblKill WHERE lotno = " & Label_LotNo_ChangeQtyInAbbatoir.Text & " AND covnert(varchar(10),[Date],120) =convert(varchar(10),getdate(),120)")
        If boolChangeQty Then
            If intCountOfAnimals <> TextBox_NewQty__ChangeQtyInAbbatoir.Text Then
                Session("Batchno") = runQuery.getScalarInt("SELECT batchno FROM empl_line WHERE lotno = " & Label_LotNo_ChangeQtyInAbbatoir.Text & " AND convert(varchar(10),Batchtime,120)=convert(varchar(10),getdate(),120)")
                ShowAlertMessage("Are you sure you want to change to qty: " & TextBox_NewQty__ChangeQtyInAbbatoir.Text & "; the killscale show: " & intCountOfAnimals & "?", "alert alert-info", "ChangeQtyInAbbatoir", True)
            End If

        End If
    End Sub
    Protected Sub Button_ChangeQtyInAnAbbatoir_Click(sender As Object, e As EventArgs) Handles Button_ChangeQtyInAnAbbatoir.Click
        ScriptManager.RegisterStartupScript(Button_ChangeQtyInAnAbbatoir, GetType(Page), "ShowInformationPanel", "openInformationPanel('informationPanel','SubmitLotNo_ChangeQtyInAbbatoir');", True)
    End Sub

    Protected Sub Button_BatchNo_RemoveLotFromLairage_Click(sender As Object, e As EventArgs) Handles Button_BatchNo_RemoveLotFromLairage.Click
        Dim intBatchno As Integer = CInt(TextBox_BatchNo_RemoveLotFromLairage.Text)
        Dim intCountRecords As Integer = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_Line WHERE batchno = " & intBatchno & " AND batchno > 8999")
        Dim intResultQtyChanged As Integer = 0
        Dim intResultChangeBatch As Integer = 0
        Dim intCancelledBatchno As Integer
        If intCountRecords > 0 Then
            intResultQtyChanged = changeQtyInBatchLive(intBatchno)
            If intResultQtyChanged = 0 Then
                ShowAlertMessage("Change Quantity Failed. Change Batchno aborted. Move to Abattoir aborted", "alert alert-danger")
                Exit Sub
            Else
                intCancelledBatchno = runQuery.getScalarInt("SELECT MAX(batchno) FROM EMPL_Line INNER JOIN EMPL_Header ON EMPL_Header.EMPLID = EMPL_Line.EMPLID WHERE Batchno BETWEEN 400 AND 500 AND convert(varchar(10),Batchtime,120)=convert(varchar(10),GetDate(),120)")
                If intCancelledBatchno = 0 Then
                    intCancelledBatchno = 400
                Else
                    intCancelledBatchno = intCancelledBatchno + 1
                End If
                intResultChangeBatch = editBatchNoLive(intBatchno, intCancelledBatchno)
                If intResultChangeBatch = 0 Then
                    ShowAlertMessage("Change Quantity Successfull. Change Batchno Failed. Move to Abattoir aborted", "alert alert-danger")
                    Exit Sub
                Else
                    runQuery.executeQuery("UPDATE EMPL_Line SET Batchno = " & intCancelledBatchno & ", AssignStatus = 3 WHERE batchno = " & intBatchno, "AMPSLindenLambConnectionString")
                    Dim intResultMoveToAbb As Integer = MoveBatchToAbb(intCancelledBatchno)
                    If intResultMoveToAbb = 0 Then
                        ShowAlertMessage("Change Quantity Successfull. Change Batchno Successfull. Move to Abattoir Failed", "alert alert-danger")
                        Exit Sub
                    Else
                        runQuery.executeQuery("UPDATE EMPL_Line SET  MoveToAbb =1 , BatchTime=GetDate(), AssignStatus = 4 WHERE batchno = " & intCancelledBatchno, "AMPSLindenLambConnectionString")
                        ShowAlertMessage("Change qty, change batchno and move to Abattoir successfull", "alert alert-danger")
                    End If

                End If
            End If
        End If
    End Sub



    Private Sub LairageListLive()
        '   Dim ws_ABLGList As New WS_SheepAbatLairageListLive.WS_SheepAbatLairageList
        Dim ws_ABLGList As New WS_SheepAbatLairageList.WS_SheepAbatLairageListPortTypeClient
        Dim ws_Generic As New WS_SheepAbatLairageList.GenericInput
        Dim ws_ABLGListInput As New WS_SheepAbatLairageList.SheepAbatLairageListInput
        Dim wsAbLGOutput As New WS_SheepAbatLairageList.SheepAbatLairageListOutput
        Dim wsListReturned() As WS_SheepAbatLairageList.AbatLairageDetail
        Dim intLooperMain As Integer
        Dim intLooper As Integer
        Dim dtResults As New Data.DataTable
        Dim drTableRow As Data.DataRow
        Dim wsArray() As String
        Dim wsArray1 As WS_SheepAbatLairageList.ArrayOfString

        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        'ws_Generic.clntRefNo_BusId = 993336
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_ABLGListInput.genericInput = ws_Generic
        ws_ABLGListInput.pinNo = "003"
        ' ws_ABLGListInput.pinNoSpecified = True
        ws_ABLGListInput.abatLairageFlag = "L"   'A = Abb  L = Lairage
        ws_ABLGListInput.includeAMPMData = "N"   'Y = Yes N = No
        ws_ABLGListInput.listAnimalsFlag = "Y"
        ws_ABLGListInput.moveDate = TextBox_Date_ViewLairageList.Text
        'ws_ABLGListInput.moveDate = checkhas2digits(Me.DTPBookDate.Value.Day) & "/" & checkhas2digits(Me.DTPBookDate.Value.Month) & "/" & Me.DTPBookDate.Value.Year
        ws_ABLGListInput.speciesCode = "OVN"

        'wsAbLGOutput = ws_ABLGList.WS_SheepAbatLairageList(ws_ABLGListInput)
        wsAbLGOutput = ws_ABLGList.WS_SheepAbatLairageList(ws_ABLGListInput)

        If wsAbLGOutput.success = 1 Then
            'Me.DataGrid1.DataSource = wsAbLGOutput.abatLairageList
            wsListReturned = wsAbLGOutput.abatLairageList

            dtResults.Columns.Add("Permit", Type.GetType("System.String"))
            dtResults.Columns.Add("Batch", Type.GetType("System.String"))
            dtResults.Columns.Add("NumAnimals", Type.GetType("System.String"))
            dtResults.Columns.Add("Type", Type.GetType("System.String")) ' EG DOA, Pen , normal
            dtResults.Columns.Add("EID", Type.GetType("System.String"))

            For intLooperMain = 0 To wsListReturned.Length - 1
                'Dead on arrival
                If wsListReturned(intLooperMain).deadOnArrival > 0 Then
                    wsArray1 = wsListReturned(intLooperMain).deadOnArrivalList
                    wsArray = wsArray1.ToArray
                    For intLooper = 0 To wsArray.Length - 1

                        drTableRow = dtResults.NewRow

                        drTableRow("Permit") = wsListReturned(intLooperMain).permitNo
                        drTableRow("Batch") = wsListReturned(intLooperMain).batchNo
                        drTableRow("NumAnimals") = wsListReturned(intLooperMain).noOfAnimals
                        drTableRow("Type") = "DOA"
                        drTableRow("EID") = wsArray(intLooper)
                        dtResults.Rows.Add(drTableRow)
                    Next

                End If

                'Dead in pen
                If wsListReturned(intLooperMain).deadInPen > 0 Then

                    wsArray1 = wsListReturned(intLooperMain).deadInPenList
                    wsArray = wsArray1.ToArray
                    For intLooper = 0 To wsArray.Length - 1

                        drTableRow = dtResults.NewRow
                        drTableRow("Permit") = wsListReturned(intLooperMain).permitNo
                        drTableRow("Batch") = wsListReturned(intLooperMain).batchNo
                        drTableRow("NumAnimals") = wsListReturned(intLooperMain).noOfAnimals
                        drTableRow("Type") = "DIP"
                        drTableRow("EID") = wsArray(intLooper)
                        dtResults.Rows.Add(drTableRow)
                    Next

                End If

                'Registered animals
                wsArray1 = wsListReturned(intLooperMain).eidList
                wsArray = wsArray1.ToArray
                intLooper = 0

                If wsArray.Length = 0 Then
                    drTableRow = dtResults.NewRow
                    drTableRow("Permit") = wsListReturned(intLooperMain).permitNo
                    drTableRow("Batch") = wsListReturned(intLooperMain).batchNo
                    drTableRow("NumAnimals") = wsListReturned(intLooperMain).noOfAnimals
                    drTableRow("Type") = "OK"
                    drTableRow("EID") = ""
                    dtResults.Rows.Add(drTableRow)
                Else

                    For intLooper = 0 To wsArray.Length - 1

                        drTableRow = dtResults.NewRow

                        drTableRow("Permit") = wsListReturned(intLooperMain).permitNo
                        drTableRow("Batch") = wsListReturned(intLooperMain).batchNo
                        drTableRow("NumAnimals") = wsListReturned(intLooperMain).noOfAnimals
                        drTableRow("Type") = "OK"
                        drTableRow("EID") = wsArray(intLooper)

                        dtResults.Rows.Add(drTableRow)

                    Next

                End If
            Next

            GridView_ViewLairageList.DataSource = dtResults
            GridView_ViewLairageList.AutoGenerateColumns = True
            GridView_ViewLairageList.DataBind()
            UpdatePanel_ViewLairageList.Update()

        Else
            ShowAlertMessage(wsAbLGOutput.error & " " & wsAbLGOutput.errorCode, "alert alert-danger")

        End If


    End Sub


    Protected Sub Button_LoadViewLairageList_Click(sender As Object, e As EventArgs) Handles Button_LoadViewLairageList.Click
        LairageListLive()
    End Sub

    Protected Sub Button_Submit_RemovePermitFromSystem_Click(sender As Object, e As EventArgs) Handles Button_Submit_RemovePermitFromSystem.Click
        Dim strPermit As String
        Dim strSQL As String
        Dim intResult As Integer
        Dim intEMPLID As Integer
        Dim AlertMessage As String = "<div class=""alert alert-warning""><strong>Warning!</strong> insertMessage </div>"
        Try
            strPermit = TextBox_PermitNo_RemovePermitFromSystem.Text
            If strPermit.Length <> 10 Then
                Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", "The permit entered is incorrect. It must be 10 characters")
            Else
                strSQL = "Select COUNT(*) FROM EMPL_header INNER JOIN EMPL_Line On EMPL_Header.EMPLID = EMPL_Line.EMPLID WHERE permitno = '" & strPermit & "' AND movetoabb = 1"
                intResult = runQuery.getScalarInt(strSQL)
                If intResult > 0 Then
                    Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", "One or more batches on this permit have entered the abattoir. You cannot cancel the permit.")
                End If

                strSQL = "SELECT COUNT(*) FROM EMPL_header WHERE permitno = '" & strPermit & "' AND empl_header.permitregistered = 0"
                intResult = runQuery.getScalarInt(strSQL)
                If intResult > 0 Then
                    Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", "This permit has not been marked as registered on APHIS. Reset cancelled.")
                Else
                    intEMPLID = runQuery.getScalarInt("SELECT emplid FROM empl_Header WHERE permitno = '" & strPermit & "'")
                    strSQL = "UPDATE EMPL_Header SET permitregistered = 0 WHERE EMPLID = " & intEMPLID
                    runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
                    strSQL = "UPDATE EMPL_Line SET permitregistered = 0, tagsverified = 0 WHERE EMPLID = " & intEMPLID
                    runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
                    strSQL = "UPDATE EMPL_Tags SET tagverified = 0, animalDead = 'NO' WHERE EMPLID = " & intEMPLID
                    runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
                    Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", "Permit reset to allow re-processing")
                End If

            End If


        Catch objError As System.Data.SqlClient.SqlException
            Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", objError.Message)
        Catch
            Label_Alert_RemovePermitFromSystem.Text = AlertMessage.Replace("insertMessage", Err.Description)
        End Try
        UpdatePanel_RemovePermitFromSystem.Update()
    End Sub
    Protected Sub Button_RemovePermitFrom_Click(sender As Object, e As EventArgs) Handles Button_RemovePermitFrom.Click
        Label_Alert_RemovePermitFromSystem.Text = String.Empty
        TextBox_PermitNo_RemovePermitFromSystem.Text = String.Empty
        UpdatePanel_RemovePermitFromSystem.Update()
    End Sub
    Protected Sub Button_Modal_ChangePermitDate_Click(sender As Object, e As EventArgs) Handles Button_Modal_ChangePermitDate.Click
        Dim strPermit As String
        Dim strSQL As String
        Dim intResult As Integer
        Dim intEMPLID As Integer
        ' Dim dteDateChange As Date
        Dim DateStr As String = TextBox_Modal_NewPermitdate_ChangePermitDate.Text
        Dim AlertMessage As String = "<div class=""alert alert-warning""><strong>Warning!</strong> insertMessage </div>"

        Try

            strPermit = TextBox_PermitNo_ChangePermitDate.Text.Trim

            If strPermit.Length <> 10 Then
                Label_ChangePermitDateAlert.Text = AlertMessage.Replace("insertMessage", "The permit entered Is incorrect. It must be 10 characters")
                Exit Sub
            Else
                strSQL = "SELECT COUNT(*) FROM EMPL_header WHERE permitno = '" & strPermit & "'"
                intResult = runQuery.getScalarInt(strSQL)

                If intResult = 0 Then
                    Label_ChangePermitDateAlert.Text = AlertMessage.Replace("insertMessage", "Permit ID " & strPermit & " not found")
                    Exit Sub
                ElseIf intResult = 1 Then
                    strSQL = "UPDATE EMPL_Header SET killdate ='" & DateStr & "' WHERE PermitNo = '" & strPermit & "'"
                    ' strSQL = "UPDATE EMPL_Header SET killdate = (SELECT convert(datetime,'" & DateStr & "',103)) WHERE PermitNo = '" & strPermit & "'"
                    runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
                    Label_ChangePermitDateAlert.Text = AlertMessage.Replace("insertMessage", "Date changed on " + strPermit + " to " + DateStr)
                    Exit Sub
                ElseIf intResult > 1 Then
                    Label_ChangePermitDateAlert.Text = AlertMessage.Replace("insertMessage", "Problem. More than one record with that permit number can't update date.")
                    Exit Sub

                End If
            End If

        Catch
            Label_ChangePermitDateAlert.Text = AlertMessage.Replace("insertMessage", Err.Description)
        End Try

    End Sub

End Class
