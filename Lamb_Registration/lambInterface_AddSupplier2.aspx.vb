﻿Imports System.Data.SqlClient
Imports System.Data
Imports PP_Primal
Imports System.Net

Partial Class lambInterface_AddSupplier2
    Inherits System.Web.UI.Page
    Dim runQuery As New SQLAccess
    Dim newSupplierID As Integer = 0
    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    Public Shared Function SearchCustomers(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim customers As List(Of String) = New List(Of String)
        Using cmd As SqlCommand = New SqlCommand("Select distinct top 50 Name from SuppLier where Name Like '" & prefixText.Replace("'", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
            cmd.Connection.Open()
            Using reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    customers.Add(reader("Name").ToString)
                End While
            End Using

            cmd.Connection.Close()
            cmd.Dispose()
        End Using
        Return customers

    End Function

    Protected Sub TextBox_Modal_SupplierKeyword_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Modal_SupplierKeyword.TextChanged
        SqlDataSource_Suppliers.DataBind()
        GridView_Modal_Suppliers.DataBind()
    End Sub
    Protected Sub GridView_Modal_Suppliers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_Modal_Suppliers.SelectedIndexChanged

        If GridView_Modal_Suppliers.SelectedIndex > -1 Then
            '  Dim SupplierID As String = GridView_Modal_Suppliers.SelectedDataKey.ToString
            If FormView_Supplier.CurrentMode = FormViewMode.Insert Then

                FormView_Supplier.ChangeMode(FormViewMode.Edit)

            End If

            'FormView_Supplier.CurrentMode = FormViewMode.Edit
            UpdatePanel_SupplierDetails.Update()
            ScriptManager.RegisterStartupScript(GridView_Modal_Suppliers, GetType(Page), "showpanel", "openInformationPanel(event,'NewSupplier');", True)
        End If
    End Sub
    Protected Sub GridView_Modal_Suppliers_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView_Modal_Suppliers.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Me.Page.ClientScript.GetPostBackClientHyperlink(GridView_Modal_Suppliers, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Sub FormView_Supplier_ItemInserting(sender As Object, e As FormViewInsertEventArgs) Handles FormView_Supplier.ItemInserting
        newSupplierID = runQuery.getScalarInt("select max([Supplier])+1 from SuppLier")
        e.Values("Supplier") = newSupplierID

    End Sub
    Protected Sub Button_Search_Click(sender As Object, e As EventArgs) Handles Button_Search.Click
        GridView_Modal_Suppliers.DataBind()

    End Sub
    Public Sub createNavisionSupplier(ByVal intSupplierNo As Integer)
        Try


            Dim VenderSQL As String = "Select 'LP'+ right((1000000+Max(CONVERT(int,REPLACE(No_,'LP','')))+1),6) From [Dynamics2009].[dbo].[Linden Foods LF8$Vendor] Where Len(No_) = 8 And Left(No_, 2) ='LP' "
            Dim NewVendorCode As String = runQuery.getScalarString(VenderSQL, "PrimalNavision")

            Dim daConnect As SqlDataAdapter
            Dim dsSupplier As New DataSet
            Dim strSQL = "Select Supplier, name, name,isnull(address1,'') as address,isnull(address2,'') as address2,isnull(address3,'') as City, isnull(addinfo,'') as postCode, isnull(Phone,'')as Phone, " &
                            "isnull(EmailAddress,'') as Email,isnull(HerdNo,'') as HerdNo,isnull(HerdNo,'') as FlockNo, isnull(Dest,'') as Dest, isnull(SFS,'0') as SFS FROM LFKILL.[AMPSLindenLamb].[dbo].[SuppLier] WHERE supplier = " & intSupplierNo

            daConnect = New SqlClient.SqlDataAdapter(strSQL, ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
            daConnect.Fill(dsSupplier, "SupplierDetails")
            daConnect.Dispose()

            Dim ppweb As New PPWebService
            ppweb.Credentials = New NetworkCredential("navedi", "linden1*", "Linden")
            Dim ppImports As New PPVendorImport
            Dim ppvendor(1) As vendor
            ppImports.vendor = ppvendor
            ppImports.vendor(0) = New vendor
            ppImports.vendor(0).VendorNo = NewVendorCode
            ppImports.vendor(0).Name = dsSupplier.Tables(0).Rows(0).Item("Name")
            ppImports.vendor(0).SearchName = dsSupplier.Tables(0).Rows(0).Item("Name")
            ppImports.vendor(0).Address = dsSupplier.Tables(0).Rows(0).Item("Address")
            ppImports.vendor(0).Address2 = dsSupplier.Tables(0).Rows(0).Item("Address2")
            ppImports.vendor(0).City = dsSupplier.Tables(0).Rows(0).Item("City")
            ppImports.vendor(0).PostCode = dsSupplier.Tables(0).Rows(0).Item("postCode")
            ppImports.vendor(0).Phone = dsSupplier.Tables(0).Rows(0).Item("Phone")
            ppImports.vendor(0).Email = dsSupplier.Tables(0).Rows(0).Item("Email")
            ppImports.vendor(0).FlockNo = dsSupplier.Tables(0).Rows(0).Item("FlockNo")
            ppImports.vendor(0).HerdNo = dsSupplier.Tables(0).Rows(0).Item("HerdNo")
            ppImports.vendor(0).VendorPostingGroup = "PPS"
            ppImports.vendor(0).VatBusPG = "UK"
            ppImports.vendor(0).GenBugPG = "GBNI"
            ppImports.vendor(0).SupplierID = dsSupplier.Tables(0).Rows(0).Item("Supplier")
            ppImports.vendor(0).SheepSupplier = True
            ppImports.vendor(0).CattleSupplier = False
            ppImports.vendor(0).PaymentTermCode = "7"
            If dsSupplier.Tables(0).Rows(0).Item("Dest") = "SFS" Then
                ppImports.vendor(0).SFS = 1
                ppImports.vendor(0).SFSNo = CInt(dsSupplier.Tables(0).Rows(0).Item("SFS"))
            Else
                ppImports.vendor(0).SFS = 0
                ppImports.vendor(0).SFSNo = CInt(dsSupplier.Tables(0).Rows(0).Item("SFS"))
            End If


            ppweb.ImportPPVendor(ppImports)
        Catch ex As Exception
            ShowAlertMessage(ex.Message)

        End Try
    End Sub
    Public Sub createInnovaSupplier(ByVal intSupplierNo As Integer)

        Dim strSQL As String
        Dim daConnect As SqlDataAdapter
        Dim dsSupplier As New DataSet
        Dim XMLString As String
        Dim strName As String
        Dim strAdminName As String
        Dim strAddress1 As String
        Dim strAddress2 As String
        Dim strAddress3 As String
        Dim strPostcode As String
        Dim strFlockNo As String

        Try
            strSQL = "Select Supplier, ISNULL(name,'') as name, ISNULL(admin_name,'') as admin_name, ISNULL(address1,'') as address1, ISNULL(address2,'') as address2, " &
                        "ISNULL(address3,'') as address3, ISNULL(addinfo,'') as addinfo, ISNULL(HerdNo,'') as Herdno FROM supplier WHERE supplier = " & intSupplierNo

            daConnect = New SqlClient.SqlDataAdapter(strSQL, ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
            daConnect.Fill(dsSupplier, "SupplierDetails")
            daConnect.Dispose()

            strName = dsSupplier.Tables(0).Rows(0).Item("Name")
            strAdminName = dsSupplier.Tables(0).Rows(0).Item("Admin_Name")
            strAddress1 = dsSupplier.Tables(0).Rows(0).Item("Address1")
            strAddress2 = dsSupplier.Tables(0).Rows(0).Item("Address2")
            strAddress3 = dsSupplier.Tables(0).Rows(0).Item("Address3")
            strPostcode = dsSupplier.Tables(0).Rows(0).Item("addinfo")
            strFlockNo = dsSupplier.Tables(0).Rows(0).Item("HerdNo")



            If strName.Trim.Length = 0 Then
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strName = strName.Replace("&", "and")
                strName = strName.Replace("'", "")
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & ">" & strName & "</Property>"
            End If

            If strAdminName.Trim.Length = 0 Then
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAdminName = strAdminName.Replace("&", "and")
                strAdminName = strAdminName.Replace("'", "")
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & ">" & strAdminName & "</Property>"
            End If

            If strAddress1.Trim.Length = 0 Then
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress1 = strAddress1.Replace("&", "and")
                strAddress1 = strAddress1.Replace("'", "")
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & ">" & strAddress1 & "</Property>"
            End If

            If strAddress2.Trim.Length = 0 Then
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress2 = strAddress2.Replace("&", "and")
                strAddress2 = strAddress2.Replace("'", "")
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & ">" & strAddress2 & "</Property>"
            End If

            If strAddress3.Trim.Length = 0 Then
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress3 = strAddress3.Replace("&", "and")
                strAddress3 = strAddress3.Replace("'", "")
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & ">" & strAddress3 & "</Property>"
            End If

            If strPostcode.Trim.Length = 0 Then
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strPostcode = strPostcode.Replace("&", "and")
                strPostcode = strPostcode.Replace("'", "")
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & ">" & strPostcode & "</Property>"
            End If

            XMLString = "<Objects>"

            XMLString += "<Object DataType=" & Chr(34) & "Marel.Mp5.Base.Database.CompanyRecord" & Chr(34) & ">" &
                    "<Properties>" &
                    "<Property name=" & Chr(34) & "Code" & Chr(34) & ">" & "#" & dsSupplier.Tables(0).Rows(0).Item("Supplier") & "</Property>" &
                    "<Property name=" & Chr(34) & "Description5" & Chr(34) & ">" & strFlockNo & "</Property>" &
                    strName &
                    strAdminName &
                    strAddress1 &
                    strAddress2 &
                    strAddress3 &
                    strPostcode &
                    "</Properties></Object></Objects>"

            strSQL = "INSERT INTO itgr_imports (regTime,importStatus,ImportHandlerCode,Data)  VALUES(getdate(),0,'igih0010','" & XMLString & "')"

            runQuery.executeInnovaNonQuery(strSQL)

        Catch ex As Exception
            Label_AlertMessage.Text = ex.Message
            UpdatePanel_Alert.Update()
            ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)
        End Try
    End Sub

    Protected Sub FormView_Supplier_ItemInserted(sender As Object, e As FormViewInsertedEventArgs) Handles FormView_Supplier.ItemInserted
        createInnovaSupplier(newSupplierID)
        createNavisionSupplier(newSupplierID)
        Dim address1 As String = CType(FormView_Supplier.FindControl("TextBox_Address1"), TextBox).Text.Replace("'", "''")
        Dim address2 As String = CType(FormView_Supplier.FindControl("TextBox_Address2"), TextBox).Text.Replace("'", "''")
        Dim address3 As String = CType(FormView_Supplier.FindControl("TextBox_Address3"), TextBox).Text.Replace("'", "''")
        Dim address4 As String = CType(FormView_Supplier.FindControl("TextBox_Address4"), TextBox).Text.Replace("'", "''")
        Dim postCode As String = CType(FormView_Supplier.FindControl("TextBox_PostCode"), TextBox).Text
        Dim FlockNo As String = CType(FormView_Supplier.FindControl("TextBox_FlockNo"), TextBox).Text
        Dim phone As String = CType(FormView_Supplier.FindControl("TextBox_Phone"), TextBox).Text
        Dim updateStr As String = "Update [SuppLier] set address1='" & address1 & "',address2='" & address2 & "',address3='" & address3 &
            "',address4='" & address4 & "',addinfo='" & postCode & "', [Type]='',Phone='" & phone & "',QA=''" &
            " where Supplier=" & newSupplierID

        runQuery.executeQuery(updateStr, "AMPSLindenLambConnectionString")
        Label_AlertMessage.Text = "Supplier is added into system."
        UpdatePanel_Alert.Update()
        ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal_AlertMessage');", True)
        'ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)
    End Sub
    Protected Sub FormView_Supplier_ItemUpdated(sender As Object, e As FormViewUpdatedEventArgs) Handles FormView_Supplier.ItemUpdated
        ' MsgBox("test")
        ' createInnovaSupplier(GridView_Modal_Suppliers.SelectedDataKey("Supplier"))
        Dim FlockNo As String = CType(FormView_Supplier.FindControl("TextBox_FlockNo"), TextBox).Text
        Dim VenderSQL As String = "Select count(*) From [Dynamics2009].[dbo].[Linden Foods LF8$Vendor] Where Len(No_) = 8 And Left(No_, 2) ='LP' and [Herd No_]='" & FlockNo & "'"
        'ShowAlertMessage("this is a test")
        If CInt(runQuery.getScalarString(VenderSQL, "PrimalNavision")) = 0 Then
            newSupplierID = GridView_Modal_Suppliers.SelectedDataKey("Supplier")

            createNavisionSupplier(newSupplierID)

        End If
    End Sub
    Sub ShowAlertMessage(ByVal Msg As String)
        Label_AlertMessage.Text = Msg
        UpdatePanel_Alert.Update()
        ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal_AlertMessage');", True)
    End Sub

End Class
