﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing

Partial Class lambInterface_bookpermits
    Inherits System.Web.UI.Page

    Dim runQuery As New SQLAccess

    Dim strSpecie As String
    Dim intDestination As Integer

    Dim boolVOAuthorisation As Boolean
    Dim boolPermitRegistered As Boolean
    Dim intUnder12Months As Integer = 0
    Dim intResideOver30Days As Integer = 0
    Dim intMax3Farms As Integer = 0

    'Dim strBatchOrTag As String
    'Dim intBatchType As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TextBox_Date.Text = Now.Date.Date.ToString("dd/MM/yyyy")
        End If
    End Sub
    Protected Sub Button_pre_Click(sender As Object, e As EventArgs) Handles Button_pre.Click
        Dim selectedDate As Date = CDate(TextBox_Date.Text)
        TextBox_Date.Text = selectedDate.AddDays(-1)
    End Sub
    Protected Sub Button_next_Click(sender As Object, e As EventArgs) Handles Button_next.Click
        Dim selectedDate As Date = CDate(TextBox_Date.Text)
        TextBox_Date.Text = selectedDate.AddDays(1)
    End Sub


    Protected Sub GridView_PermitNos_DataBound(sender As Object, e As EventArgs) Handles GridView_PermitNos.DataBound

        CalcBookingTotals_Lairage()
        CalcBookingTotals_Abattoir()
        Label_TotalLambs.Text = CInt(lb_TotalLair.Text) + CInt(lb_TotalAbb.Text)
    End Sub
    Private Sub CalcBookingTotals_Lairage()
        'caz sql server lfkill is on a old version, so we need to get the right format the date in the sql query
        Dim datetimeStrings As String() = TextBox_Date.Text.Split("/")
        Dim strSQL As String = "SELECT NAME, (SELECT ISNULL(SUM(EMPL_Line.NewQty-EMPL_Line.DeadInPen-EMPL_Line.DeadOnArrival),0) FROM EMPL_Header INNER JOIN " &
                                "EMPL_Line ON EMPL_Header.EMPLID = EMPL_Line.EMPLID WHERE EMPL_Header.killDate <= '" & datetimeStrings(2) & "-" & datetimeStrings(1) & "-" & datetimeStrings(0) & "' AND EMPL_Line.MoveToAbb = 0 AND specie = sx1.ec_code) AS numBooked FROM  sex Sx1 ORDER BY ec_Code"
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
            conn.Open()
            Using cmd As SqlCommand = New SqlCommand(strSQL, conn)
                Using reader As SqlDataReader = cmd.ExecuteReader
                    Dim LambTotal As Integer = 0
                    While reader.Read
                        LambTotal = LambTotal + CInt(reader("numBooked"))
                        Select Case reader("NAME")
                            Case "Lamb"
                                lb_LambLair.Text = reader("numBooked")
                            Case "Lamb Organic"
                                lb_OrganicLair.Text = reader("numBooked")
                            Case "Lamb MS"
                                lb_RissLair.Text = reader("numBooked")
                            Case "Hogget"
                                lb_HoggetLair.Text = reader("numBooked")
                            Case "Ewe"
                                lb_EweLair.Text = reader("numBooked")
                            Case "Ram"
                                lb_RamLair.Text = reader("numBooked")
                        End Select
                    End While
                    lb_TotalLair.Text = LambTotal
                End Using
            End Using
        End Using
    End Sub

    Private Sub CalcBookingTotals_Abattoir()
        'caz sql server lfkill is on a old version, so we need to get the right format the date in the sql query

        Dim strSQL As String = " SELECT NAME, (select isnull(SUM(EMPL_Line.NewQty-EMPL_Line.DeadInPen-EMPL_Line.DeadOnArrival),0) from EMPL_Header INNER JOIN " &
                        "EMPL_Line ON EMPL_Header.EMPLID = EMPL_Line.EMPLID WHERE convert(varchar(10),EMPL_Line.batchTime,103)='" & TextBox_Date.Text & "' AND specie = sx1.ec_code) AS numBooked FROM  sex Sx1 ORDER BY ec_Code"
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
            conn.Open()
            Using cmd As SqlCommand = New SqlCommand(strSQL, conn)
                Using reader As SqlDataReader = cmd.ExecuteReader
                    Dim LambTotal As Integer = 0
                    While reader.Read
                        LambTotal = LambTotal + CInt(reader("numBooked"))
                        Select Case reader("NAME")
                            Case "Lamb"
                                lb_LambAbb.Text = reader("numBooked")
                            Case "Lamb Organic"
                                lb_OrganicAbb.Text = reader("numBooked")
                            Case "Lamb MS"
                                lb_RissAbb.Text = reader("numBooked")
                            Case "Hogget"
                                lb_HoggetAbb.Text = reader("numBooked")
                            Case "Ewe"
                                lb_EweAbb.Text = reader("numBooked")
                            Case "Ram"
                                lb_RamAbb.Text = reader("numBooked")
                        End Select
                    End While
                    lb_TotalAbb.Text = LambTotal
                End Using
            End Using
        End Using
    End Sub
    Private Sub FillForm(ByVal EMPLID As String)
        Dim strSQL As String
        Dim daConnect As SqlClient.SqlDataAdapter
        Dim dsHeaderData As New DataSet
        Dim boolPermitRegistered As Boolean = False

        strSQL = "SELECT EMPL_Header.EMPLID, EMPL_Header.PermitNo, EMPL_Header.FlockNo, EMPL_Header.Suppno, isnull(SuppLier.NAME,'') as supplierName, EMPL_Header.BatchType,  " &
                   "EMPL_Header.QA, EMPL_Header.Company, EMPL_Header.FlockId, EMPL_Header.CertificateNo, EMPL_Header.DirectImportFlock, " &
                   "EMPL_Header.PortFlock,BatchOrTag, isnull(address1,'') as addr1,isnull(address2,'') as addr2, isnull(address3 +  ' ' + address4,'') as addr3, PermitRegistered, VOAuthorisation,isnull(emailaddress,'') as emailaddress " &
                   "FROM  EMPL_Header LEFT OUTER JOIN SuppLier ON EMPL_Header.Suppno = SuppLier.Supplier WHERE EMPLID = " & EMPLID
        'isnull(address1 + ' ' + address2,'') as addr1, isnull(address3 +  ' ' + address4,'') as addr2
        daConnect = New SqlClient.SqlDataAdapter(strSQL, ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
        daConnect.Fill(dsHeaderData, "HeaderData")
        daConnect.Dispose()
        TextBox_Modal_PermitNo.Text = dsHeaderData.Tables(0).Rows(0).Item("permitno")
        TextBox_Modal_Flock.Text = dsHeaderData.Tables(0).Rows(0).Item("flockno")
        Label_Modal_SupplierName.Text = dsHeaderData.Tables(0).Rows(0).Item("supplierName")
        Label_Modal_Address1.Text = dsHeaderData.Tables(0).Rows(0).Item("Addr1")
        Label_Modal_Address2.Text = dsHeaderData.Tables(0).Rows(0).Item("Addr2")
        Label_Modal_Address3.Text = dsHeaderData.Tables(0).Rows(0).Item("Addr3")
        TextBox_Modal_Email.Text = dsHeaderData.Tables(0).Rows(0).Item("emailAddress")
        Label_boolPermitRegistered.Text = dsHeaderData.Tables(0).Rows(0).Item("PermitRegistered")
        boolPermitRegistered = dsHeaderData.Tables(0).Rows(0).Item("PermitRegistered")
        Label_boolVOAuthorisation.Text = dsHeaderData.Tables(0).Rows(0).Item("VOAuthorisation")
        Label_Modal_SupplierNo.Text = dsHeaderData.Tables(0).Rows(0).Item("Suppno")

        If dsHeaderData.Tables(0).Rows(0).Item("QA") = True Then
            CheckBox_Modal_QA.Checked = True
        Else
            CheckBox_Modal_QA.Checked = False
        End If

        Label_strBatchOrTag.Text = dsHeaderData.Tables(0).Rows(0).Item("BatchOrTag")
        changeBatchType(dsHeaderData.Tables(0).Rows(0).Item("BatchType"))
        Label_intBatchType.Text = dsHeaderData.Tables(0).Rows(0).Item("BatchType")


        'If boolPermitRegistered Then
        '    TextBox_Modal_PermitNo.Enabled = False
        '    TextBox_Modal_Flock.Enabled = False


        'End If


        ' intSupplier = dsHeaderData.Tables(0).Rows(0).Item("Suppno")
        'If strBatchOrTag = "BATCH" Then
        '    Me.btnBatch1.BackColor = Color.ForestGreen
        '    Me.btnTag1.BackColor = Color.DarkRed
        'Else
        '    Me.btnBatch1.BackColor = Color.DarkRed
        '    Me.btnTag1.BackColor = Color.ForestGreen
        'End If

        '    TextBox_Modal_Certificate.Text = dsHeaderData.Tables(0).Rows(0).Item("CertificateNo")

    End Sub
    Private Sub changeBatchType(ByVal intType As Integer)

        Select Case intType
            Case 1
                setButtonsRed()
                Button_Modal_Farm.BackColor = Color.ForestGreen
                Label_intBatchType.Text = 1
            Case 2
                setButtonsRed()
                Button_Modal_Mart.BackColor = Color.ForestGreen
                Label_intBatchType.Text = 2
        End Select

    End Sub
    Private Sub setButtonsRed()
        Button_Modal_Farm.BackColor = Color.DarkRed
        Button_Modal_Mart.BackColor = Color.DarkRed
    End Sub
    Sub resetModal()
        TextBox_Modal_PermitNo.Enabled = True
        TextBox_Modal_Flock.Enabled = True
        TextBox_Modal_Email.Enabled = True
        TextBox_Modal_PermitNo.Text = String.Empty
        TextBox_Modal_Flock.Text = String.Empty
        TextBox_Modal_Email.Text = String.Empty
        Label_Modal_SupplierNo.Text = String.Empty

        Label_Modal_SupplierName.Text = String.Empty
        Label_Modal_Address1.Text = String.Empty
        Label_Modal_Address2.Text = String.Empty

        'DropDownList_Modal_Company.Enabled = True
        Button_Modal_Flock.Enabled = True
        Button_Modal_Quick.Enabled = True
        Button_Modal_Add.Enabled = True
        Button_Modal_Delete.Enabled = True
        Button_Modal_ChangeDest.Enabled = True
        Button_Modal_Mart.Enabled = True
        Button_Modal_Farm.Enabled = True
        '   Button_Modal_DirectImport.Enabled = True
        '  Button_Modal_PortImport.Enabled = True
    End Sub
    Protected Sub GridView_PermitNos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_PermitNos.SelectedIndexChanged
        resetModal()
        Label_EMPLID.Text = GridView_PermitNos.SelectedDataKey.Value
        FillForm(GridView_PermitNos.SelectedDataKey.Value)
        TextBox_Modal_PermitNo.Enabled = False
        TextBox_Modal_Flock.Enabled = False
        TextBox_Modal_Email.Enabled = False
        '   DropDownList_Modal_Company.Enabled = False
        Button_Modal_Flock.Enabled = False
        Button_Modal_Quick.Enabled = False
        '   Button_Modal_Add.Enabled = False
        '  Button_Modal_Delete.Enabled = False
        Button_Modal_Mart.Enabled = False
        Button_Modal_Farm.Enabled = False


        If GridView_PermitBatches.Rows.Count <= 0 Then
            Button_Modal_Delete.Enabled = False
            Button_Modal_ChangeDest.Enabled = False
        End If

        '  Button_Modal_DirectImport.Enabled = False
        '  Button_Modal_PortImport.Enabled = False
        UpdatePanel_Modal_PermitAndBatch.Update()
    End Sub
    Protected Sub Button_Modal_Edit_Click(sender As Object, e As EventArgs) Handles Button_Modal_Edit.Click
        TextBox_Modal_Email.Enabled = True
        UpdatePanel_Modal_PermitAndBatch.Update()
    End Sub
    Protected Sub Button_NewLot_Click(sender As Object, e As EventArgs) Handles Button_NewLot.Click
        Dim strSQL As String
        Dim intNextEMPLID As Integer = runQuery.getScalarInt("SELECT MAX(EMPLID) + 1 FROM EMPL_Header")
        ' MsgBox(intNextEMPLID)
        strSQL = "INSERT INTO EMPL_Header (EMPLID,KillDate) VALUES (" & intNextEMPLID & ",GetDate())"
        Label_EMPLID.Text = intNextEMPLID
        resetModal()

        'enable below code to add new EMPL_Header
        runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
        FillForm(intNextEMPLID)
        UpdatePanel_Modal_PermitAndBatch.Update()
    End Sub
    Sub UpdateBatch(ByVal EMPLID As Integer, ByVal EMPLLineID As Integer, ByVal Specie As String, ByVal intUnder12Months As Integer, ByVal intResideOver30Days As Integer, ByVal intMax3Farms As Integer)
        Dim strDest As String = runQuery.getScalarString("select Isnull(Dest,'') as Dest from supplier inner join [EMPL_Header] on supplier.Supplier=[EMPL_Header].Suppno where emplid= " & EMPLID, "AMPSLindenLambConnectionString")
        Dim intMS As Integer = 0
        Dim intTesco As Integer = 0
        'Dim intUnder12Months As Integer = 0
        'Dim intResideOver30Days As Integer = 0
        'Dim intMax3Farms As Integer = 0

        If CheckBox_Modal_QA.Checked Then
            intTesco = 1
            If strDest = "SFS" Then
                intMS = 1
            End If
        End If

        Dim strSQL As String = "UPDATE EMPL_Line SET U12M = " & intUnder12Months & ", O30DaysReside= " & intResideOver30Days & ", Farms_3OrLess = " & intMax3Farms & ", MS = " & intMS & ", Tesco= " & intTesco &
            ", company = " & DropDownList_Model_PickSpecie_Destination.SelectedValue & ", specie = '" & Specie & "' WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
        runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")


        Label_Modal_PickSpecie_New_Update.Text = ""
        SqlDataSource_PermitBatches.DataBind()
        GridView_PermitBatches.DataBind()
        Label_BatchCount.Text = GridView_PermitBatches.Rows.Count
        UpdatePanel_Modal_PermitAndBatch.Update()
        ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "hideMyModal", "HideModal('#myModal_NewBatch');", True)
        ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)

    End Sub
    Sub AddNewBatch(ByVal EMPLID As Integer, ByVal Specie As String, ByVal intUnder12Months As Integer, ByVal intResideOver30Days As Integer, ByVal intMax3Farms As Integer, ByVal VO As Boolean)

        Dim strSQL As String
        If VO = False Then
            '  If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = False Then

            Dim strDest As String = runQuery.getScalarString("select Isnull(Dest,'') as Dest from supplier inner join [EMPL_Header] on supplier.Supplier=[EMPL_Header].Suppno where emplid= " & EMPLID, "AMPSLindenLambConnectionString")
            Dim intMS As Integer = 0
            Dim intTesco As Integer = 0
            'Dim intUnder12Months As Integer = 0
            'Dim intResideOver30Days As Integer = 0
            'Dim intMax3Farms As Integer = 0

            If CheckBox_Modal_QA.Checked Then
                intTesco = 1
                If strDest = "SFS" Then
                    intMS = 1
                End If
            End If


            Dim intNextEMPLLineID As Integer = runQuery.getScalarInt("SELECT isnull(MAX(EMPLLineID),0)+1 FROM EMPL_Line WHERE EMPLID=" & Label_EMPLID.Text, "AMPSLindenLambConnectionString")
            Dim intNextTempBatchNo As Integer = runQuery.getScalarInt("SELECT MAX(batchno) FROM empl_line WHERE emplid > (SELECT MAX(emplid)-200 FROM empl_line)", "AMPSLindenLambConnectionString")
            If intNextTempBatchNo >= 999 Then
                intNextTempBatchNo = runQuery.getScalarInt("SELECT MAX(batchno) FROM empl_line WHERE emplid> (SELECT MAX(emplid)-200 FROM empl_line) AND batchno <500", "AMPSLindenLambConnectionString")
                If intNextTempBatchNo = 0 Then
                    intNextTempBatchNo = 100
                Else
                    intNextTempBatchNo += 1
                End If
            Else
                intNextTempBatchNo += 1
            End If

            strSQL = "INSERT INTO EMPL_Line (EMPLID,EMPLLineID, Specie, Batchno,Company,U12M, O30DaysReside, Farms_3OrLess,MS,Tesco) VALUES(" & EMPLID & ", " & intNextEMPLLineID & ",'" &
                    Specie & "', " & intNextTempBatchNo & ", " & intDestination & ", " & intUnder12Months & ", " & intResideOver30Days & ", " & intMax3Farms & "," & intMS & "," & intTesco & ")"
            runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
            SqlDataSource_PermitBatches.SelectCommand = "SELECT EMPLID,EMPLLineID,specie,qty,NewQty,tagFrom,tagto,tagColour,EMPLGroup,batchno,martlotno,movetoabb,Company,permitRegistered, QA, destination.description as dest FROM EMPL_Line " &
                                                "INNER JOIN sex On sex.ec_code = EMPL_line.specie INNER JOIN destination On destination.destno = EMPL_Line.company WHERE  EMPLID =" & EMPLID & " And moveToAbb =0"
            SqlDataSource_PermitBatches.DataBind()
            GridView_PermitBatches.DataBind()
            Label_BatchCount.Text = GridView_PermitBatches.Rows.Count
            UpdatePanel_Modal_PermitAndBatch.Update()
            ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "hideMyModal", "HideModal('#myModal_NewBatch');", True)
            ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "showMyModal", "ShowModal('#myModal');", True)
            ' ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "AlertMyModal", "Modal_AlertMessage('Label_Modal_AlertMessage','Please have VOs cleared on this permit');", True)
        Else
            ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "AlertMyModal", "Modal_AlertMessage('Label_Modal_AlertMessage','Please have VOs cleared on this permit');", True)
        End If
    End Sub


    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    Public Shared Function SearchCustomers(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim customers As List(Of String) = New List(Of String)
        Using cmd As SqlCommand = New SqlCommand("Select distinct top 50 Name from SuppLier where Name Like '" & prefixText.Replace("'", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
            cmd.Connection.Open()
            Using reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    customers.Add(reader("Name").ToString)
                End While
            End Using

            cmd.Connection.Close()
            cmd.Dispose()
        End Using
        Return customers

    End Function
    Protected Sub TextBox_Modal_SupplierKeyword_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Modal_SupplierKeyword.TextChanged
        SqlDataSource_Suppliers.DataBind()
        GridView_Modal_Suppliers.DataBind()
    End Sub

    Protected Sub Button_Enter_Click(sender As Object, e As EventArgs) Handles Button_Enter.Click
        UpdatePanel_informationPanel_Supllier.Update()
    End Sub

    Protected Sub GridView_Modal_Suppliers_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView_Modal_Suppliers.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Me.Page.ClientScript.GetPostBackClientHyperlink(Me.GridView_Modal_Suppliers, "Select$" & e.Row.RowIndex)
        End If
    End Sub
    Protected Sub GridView_PermitBatches_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView_PermitBatches.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Me.Page.ClientScript.GetPostBackClientHyperlink(Me.GridView_PermitBatches, "Select$" & e.Row.RowIndex)
        End If
    End Sub
    Protected Sub GridView_Modal_Suppliers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_Modal_Suppliers.SelectedIndexChanged
        TextBox_Modal_Flock.Text = GridView_Modal_Suppliers.SelectedRow.Cells(1).ToString()
        Dim supplierID As String = GridView_Modal_Suppliers.SelectedDataKey.Value

        loadSupplierDetails("Supplier", supplierID)

        ScriptManager.RegisterStartupScript(GridView_Modal_Suppliers, GetType(Page), "showpanel", "openInformationPanel(event,'PermitandBatch');", True)
        'If (Not ClientScript.IsStartupScriptRegistered("showpanel")) Then
        '    Me.Page.ClientScript.RegisterStartupScript(Me.GetType, "showpanel", "openInformationPanel(event,'PermitandBatch');", True)
        'End If

    End Sub
    Protected Sub Button_Modal_Quick_Click(sender As Object, e As EventArgs) Handles Button_Modal_Quick.Click
        If TextBox_Modal_Flock.Text <> "" Then
            loadSupplierDetails("HerdNo", "'" & TextBox_Modal_Flock.Text & "'")
        End If
    End Sub
    Sub loadSupplierDetails(ByVal Key As String, ByVal keyvalue As String)
        Dim strSQL As String
        Dim daConnect As SqlClient.SqlDataAdapter
        Dim dsSupllier As New DataSet
        strSQL = "SELECT Supplier, HerdNo, isnull(NAME,'') as supplierName,  " &
                   "isnull(address1 + ' ' + address2,'') as addr1, isnull(address3 +  ' ' + address4,'') as addr2, isnull(emailaddress,'') as emailaddress " &
                   "FROM  SuppLier WHERE " & Key & " = " & keyvalue
        daConnect = New SqlClient.SqlDataAdapter(strSQL, ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString)
        daConnect.Fill(dsSupllier, "Supplierdata")
        daConnect.Dispose()
        resetModal()

        Label_Modal_SupplierNo.Text = dsSupllier.Tables(0).Rows(0).Item("Supplier")
        TextBox_Modal_Flock.Text = dsSupllier.Tables(0).Rows(0).Item("HerdNo")
        Label_Modal_SupplierName.Text = dsSupllier.Tables(0).Rows(0).Item("supplierName")
        Label_Modal_Address1.Text = dsSupllier.Tables(0).Rows(0).Item("Addr1")
        Label_Modal_Address2.Text = dsSupllier.Tables(0).Rows(0).Item("Addr2")
        TextBox_Modal_Email.Text = dsSupllier.Tables(0).Rows(0).Item("emailAddress")
        UpdatePanel_Modal_PermitAndBatch.Update()
    End Sub
    Protected Sub Button_Modal_Flock_Click(sender As Object, e As EventArgs) Handles Button_Modal_Flock.Click

    End Sub
    Protected Sub DropDownList_ModelDestination_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Protected Sub Button_Modal_PickSpecie_Lamb_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_Lamb.Click
        strSpecie = "L"


        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If
        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If

    End Sub
    Protected Sub Button_Modal_PickSpecie_OrganicLamb_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_OrganicLamb.Click
        strSpecie = "LO"
        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If

        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If
    End Sub
    Protected Sub Button_Modal_PickSpecie_RissLamb_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_RissLamb.Click
        strSpecie = "LMS"
        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If

        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If

    End Sub
    Protected Sub Button_Modal_PickSpecie_Hogget_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_Hogget.Click
        strSpecie = "H"
        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If

        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If

    End Sub
    Protected Sub Button_Modal_PickSpecie_Ram_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_Ram.Click
        strSpecie = "R"
        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If

        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If

    End Sub
    Protected Sub Button_Modal_PickSpecie_Eve_Click(sender As Object, e As EventArgs) Handles Button_Modal_PickSpecie_Eve.Click
        strSpecie = "E"
        If CheckBox_12months.Checked Then
            intUnder12Months = 1
        End If
        If CheckBox_30days.Checked Then
            intResideOver30Days = 1
        End If
        If CheckBox_3farm.Checked Then
            intMax3Farms = 1
        End If
        If Convert.ToBoolean(Label_boolVOAuthorisation.Text) = True Then
            boolVOAuthorisation = True
        Else
            boolVOAuthorisation = False
        End If

        If Label_Modal_PickSpecie_New_Update.Text = "Update" Then
            UpdateBatch(CInt(Label_EMPLID.Text), CInt(GridView_PermitBatches.SelectedDataKey(1)), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms)
        Else
            AddNewBatch(CInt(Label_EMPLID.Text), strSpecie, intUnder12Months, intResideOver30Days, intMax3Farms, boolVOAuthorisation)
        End If

    End Sub


    Protected Sub DropDownList_Model_PickSpecie_Destination_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList_Model_PickSpecie_Destination.SelectedIndexChanged
        intDestination = DropDownList_Model_PickSpecie_Destination.SelectedValue
    End Sub

    Protected Sub Button_Modal_Update_Click(sender As Object, e As EventArgs) Handles Button_Modal_Update.Click
        Try
            Dim strSQL As String = "Update EMPL_Header SET PermitNo ='" & TextBox_Modal_PermitNo.Text & "',FlockNo='" & TextBox_Modal_Flock.Text & "',suppno=" & Label_Modal_SupplierNo.Text & "," &
                "BatchType='" & Label_intBatchType.Text & "', BatchOrTag='" & Label_strBatchOrTag.Text & "' where EMPLID=" & Label_EMPLID.Text
            ''  MsgBox(strSQL)
            runQuery.executeQuery(strSQL, "AMPSLindenLambConnectionString")
            runQuery.executeQuery("UPDATE supplier SET emailaddress = '" & TextBox_Modal_Email.Text & "' WHERE supplier = " & Label_Modal_SupplierNo.Text, "AMPSLindenLambConnectionString")
            'get number of lambs that haven't been kill off to APHIS
            Dim hasLambNeededKillOff As Integer = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_Line WHERE EMPLID = " & Label_EMPLID.Text & " AND PermitRegistered = 0 and Qty>0", "AMPSLindenLambConnectionString")
            If hasLambNeededKillOff > 0 Then
                ' SheepMoveToLairage(Label_EMPLID.Text, TextBox_Modal_Flock.Text, TextBox_Modal_PermitNo.Text)
            End If
            '   Label_Modal_AlertMessage.Text = "Update Completed"
            ScriptManager.RegisterStartupScript(Button_Modal_Update, GetType(Page), "showMyAlertModal", "Modal_AlertMessageSuccess('Label_Modal_AlertSuccess','" & "Update Completed" & "');", True)

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Button_Modal_Update, GetType(Page), "showMyAlertModal", "Modal_AlertMessage('Label_Modal_AlertMessage','" & "Update Completed" & "');", True)
            'ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "AlertMyModal", "Modal_AlertMessage('Label_Modal_AlertMessage','" & "Error: " & ex.Message & "');", True)
        End Try


    End Sub
    Function SheepMoveToLairage(ByVal EMPLID As String, ByVal FlockNo As String, ByVal PermitNo As String) As Boolean
        Dim result As Boolean = False
        Dim strSQL As String
        Dim daConnectBatches As SqlDataAdapter
        Dim dsBatches As New DataSet
        Dim unRegisterBatchNo As Integer


        'table(0)
        strSQL = "SELECT EMPLID,EMPLLineID,specie,qty,NewQty,tagFrom,tagto,tagColour,EMPLGroup,batchno,martlotno,movetoabb,Company,permitRegistered,QA " &
                    "FROM EMPL_Line INNER JOIN sex on sex.ec_code = EMPL_line.specie WHERE  EMPLID = " & EMPLID & " AND PermitRegistered = 0"
        daConnectBatches = New SqlClient.SqlDataAdapter(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
        daConnectBatches.Fill(dsBatches, "BatchDetails")
        daConnectBatches.Dispose()


        'table(1)
        strSQL = "SELECT emplgroup,SUM(newqty) as qtyInGroup FROM empl_line INNER JOIN sex on sex.ec_code= specie WHERE emplid= " & EMPLID & " AND PermitRegistered = 0 GROUP BY emplgroup"
        daConnectBatches = New SqlClient.SqlDataAdapter(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
        daConnectBatches.Fill(dsBatches, "EMPLGroupDetails")
        daConnectBatches.Dispose()

        Dim ws_MTL_SheepDetailArray(dsBatches.Tables(1).Rows.Count - 1) As WS_SheepMoveToLairageLive.SheepDetails

        strSQL = "SELECT COUNT(*) FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND PermitRegistered = 0"
        unRegisterBatchNo = runQuery.getScalarInt(strSQL, "AMPSLindenLambConnectionString")
        Dim ws_MTL_SheepBatchArray(unRegisterBatchNo - 1) As WS_SheepMoveToLairageLive.SheepBatches

        For i As Integer = 0 To dsBatches.Tables(1).Rows.Count - 1
            Dim ws_MTL_SheepDetail As New WS_SheepMoveToLairageLive.SheepDetails
            ws_MTL_SheepDetail.noOfAnimals = dsBatches.Tables(1).Rows(i).Item("qtyInGroup")
            ws_MTL_SheepDetail.type = dsBatches.Tables(1).Rows(i).Item("EMPLGroup")
            ws_MTL_SheepDetailArray(i) = ws_MTL_SheepDetail
        Next

        For i As Integer = 0 To dsBatches.Tables(0).Rows.Count - 1
            If dsBatches.Tables(0).Rows(i).Item("PermitRegistered") = False Then
                Dim ws_MTL_SheepBatch As New WS_SheepMoveToLairageLive.SheepBatches
                ws_MTL_SheepBatch.noOfAnimals = dsBatches.Tables(0).Rows(i).Item("Newqty")
                ws_MTL_SheepBatch.batchNo = dsBatches.Tables(0).Rows(i).Item("Batchno")
                If dsBatches.Tables(0).Rows(i).Item("MartLotNo") <> 0 Then
                    ws_MTL_SheepBatch.lotNo = dsBatches.Tables(0).Rows(i).Item("MartLotNo")
                End If
                ws_MTL_SheepBatchArray(i) = ws_MTL_SheepBatch
            End If
        Next

        Dim ws_Generic As New WS_SheepMoveToLairageLive.GenericInput
        Dim ws_MTL_InfoInput As New WS_SheepMoveToLairageLive.SheepMoveToLairageInput
        Dim ws_MTL_InfoOutput As New WS_SheepMoveToLairageLive.SheepMoveToLairageOutput
        Dim ws_MTL As New WS_SheepMoveToLairageLive.WS_SheepMoveToLairagePortTypeClient
        ws_MTL_InfoInput.sheepBatchList = ws_MTL_SheepBatchArray
        ws_MTL_InfoInput.movementType = "EID"

        ws_Generic.callingLocation = "Meat"
        ws_Generic.channel = "Meat"
        ws_Generic.clntRefNo_BusId = 922225
        ws_Generic.environment = "Live"
        ws_Generic.herdNo = "A04001"
        ws_Generic.password = ""
        ws_Generic.username = "LINS01"

        ws_MTL_InfoInput.genericInput = ws_Generic
        ws_MTL_InfoInput.pinNo = "003"
        ws_MTL_InfoInput.noOfAnimals = unRegisterBatchNo
        ws_MTL_InfoInput.fromFlockNo = FlockNo
        ws_MTL_InfoInput.permitNo = PermitNo

        ws_MTL_InfoOutput = ws_MTL.WS_SheepMoveToLairage(ws_MTL_InfoInput)
        If ws_MTL_InfoOutput.success = 1 Then
            result = True
        Else
            ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "AlertMyModal", "Modal_AlertMessage('Label_Modal_AlertMessage','" & "Error: " & ws_MTL_InfoOutput.error & " ErrorCode: " & ws_MTL_InfoOutput.errorCode & "');", True)
        End If
        Return result
    End Function
    Protected Sub GridView_PermitBatches_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_PermitBatches.SelectedIndexChanged
        If GridView_PermitBatches.SelectedIndex <> -1 Then
            Button_Modal_ChangeDest.Enabled = True
        End If

    End Sub
    Protected Sub Button_Modal_Delete_Click(sender As Object, e As EventArgs) Handles Button_Modal_Delete.Click

        If GridView_PermitBatches.SelectedIndex <> -1 Then
            GridView_PermitBatches.DeleteRow(GridView_PermitBatches.SelectedIndex)
        End If

    End Sub
    Protected Sub Button_Modal_ChangeDest_Click(sender As Object, e As EventArgs) Handles Button_Modal_ChangeDest.Click

        If GridView_PermitBatches.SelectedIndex <> -1 Then
            Dim EMPLID As String = GridView_PermitBatches.SelectedDataKey(0)
            Dim EMPLLineID As String = GridView_PermitBatches.SelectedDataKey(1)
            Dim Company As Integer = GridView_PermitBatches.SelectedDataKey(2)
            Dim intRecordCount As Integer = runQuery.getScalarInt("SELECT COUNT(*) FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID & " AND Lotno >0", "AMPSLindenLambConnectionString")
            Dim strSQL As String
            If intRecordCount > 0 Then
                ScriptManager.RegisterStartupScript(Button_Modal_PickSpecie_OrganicLamb, GetType(Page), "AlertMyModal", "Modal_AlertMessage('Label_Modal_AlertMessage','You cannot change the destination of a lot after it has moved to the abattoir');", True)
            Else
                If Company <> "99" Then
                    DropDownList_Model_PickSpecie_Destination.SelectedIndex = DropDownList_Model_PickSpecie_Destination.Items.IndexOf(DropDownList_Model_PickSpecie_Destination.Items.FindByValue(Company))
                    strSQL = "SELECT U12M FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
                    '    intUnder12Months = runQuery.getScalarInt(strSQL, "AMPSLindenLambConnectionString")
                    If runQuery.getScalarInt(strSQL, "AMPSLindenLambConnectionString") = 1 Then
                        CheckBox_12months.Checked = True
                    Else
                        CheckBox_12months.Checked = False
                    End If

                    strSQL = "SELECT O30DaysReside FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
                    If runQuery.getScalarInt(strSQL, "AMPSLindenLambConnectionString") = 1 Then
                        CheckBox_30days.Checked = True
                    Else
                        CheckBox_30days.Checked = False
                    End If

                    strSQL = "SELECT Farms_3OrLess FROM EMPL_Line WHERE EMPLID = " & EMPLID & " AND EMPLLineID = " & EMPLLineID
                    If runQuery.getScalarInt(strSQL, "AMPSLindenLambConnectionString") = 1 Then
                        CheckBox_3farm.Checked = True
                    Else
                        CheckBox_3farm.Checked = False
                    End If

                    Label_Modal_PickSpecie_New_Update.Text = "Update"
                    UpdatePanel_PickSpecie.Update()
                End If

            End If
        End If
    End Sub



End Class
