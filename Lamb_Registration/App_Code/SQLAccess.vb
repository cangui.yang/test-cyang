﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class SQLAccess



    Public Sub executeQuery(ByVal strSQL As String, ByVal connectionstring As String)
        'Optional ByVal connectionstring As String = "AMPSLindenLambConnectionString"
        Try
            Using cmd As SqlCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings(connectionstring).ToString))
                cmd.Connection.Open()
                cmd.ExecuteNonQuery()
                cmd.Connection.Close()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            MsgBox("execute Query:-" & strSQL & "-" & ex.Message)
        End Try

    End Sub


    Public Function getScalarInt(ByVal strSQL As String, Optional ByVal connectionstring As String = "AMPSLindenLambConnectionString") As Integer

        Dim intResult As Integer

        Try
            Using cmd As SqlCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings(connectionstring).ToString))
                cmd.Connection.Open()
                intResult = Convert.ToInt32(cmd.ExecuteScalar())
                cmd.Connection.Close()
                cmd.Dispose()


            End Using
        Catch ex As Exception
            MsgBox("GetScalarInt:-" & strSQL & "-" & ex.Message)
        End Try


        Return intResult

    End Function


    Public Function getScalarString(ByVal strSQL As String, ByVal connectionstring As String) As String
        'Optional ByVal connectionstring As String = "AMPSLindenLambConnectionString"
        Dim intResult As String = ""

        Try
            Using cmd As SqlCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings(connectionstring).ToString))
                cmd.Connection.Open()
                intResult = Convert.ToString(cmd.ExecuteScalar())
                cmd.Connection.Close()
                cmd.Dispose()


            End Using
        Catch ex As Exception
            MsgBox("GetScalarString:-" & strSQL & "-" & ex.Message)
        End Try


        Return intResult

    End Function

    Public Sub createInnovaSupplier(ByVal intSupplierNo As Integer)

        Dim strSQL As String
        Dim daConnect As SqlDataAdapter
        Dim dsSupplier As New Data.DataSet
        Dim XMLString As String
        Dim strName As String
        Dim strAdminName As String
        Dim strAddress1 As String
        Dim strAddress2 As String
        Dim strAddress3 As String
        Dim strPostcode As String
        Dim strFlockNo As String

        Try


            strSQL = "SELECT Supplier, ISNULL(name,'') as name, ISNULL(admin_name,'') as admin_name, ISNULL(address1,'') as address1, ISNULL(address2,'') as address2, " &
                        "ISNULL(address3,'') as address3, ISNULL(addinfo,'') as addinfo, ISNULL(HerdNo,'') as Herdno FROM supplier WHERE supplier = " & intSupplierNo

            daConnect = New SqlDataAdapter(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
            daConnect.Fill(dsSupplier, "SupplierDetails")
            daConnect.Dispose()

            strName = dsSupplier.Tables(0).Rows(0).Item("Name")
            strAdminName = dsSupplier.Tables(0).Rows(0).Item("Admin_Name")
            strAddress1 = dsSupplier.Tables(0).Rows(0).Item("Address1")
            strAddress2 = dsSupplier.Tables(0).Rows(0).Item("Address2")
            strAddress3 = dsSupplier.Tables(0).Rows(0).Item("Address3")
            strPostcode = dsSupplier.Tables(0).Rows(0).Item("addinfo")
            strFlockNo = dsSupplier.Tables(0).Rows(0).Item("HerdNo")



            If strName.Trim.Length = 0 Then
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strName = strName.Replace("&", "and")
                strName = strName.Replace("'", "")
                strName = "<Property name=" & Chr(34) & "Name" & Chr(34) & ">" & strName & "</Property>"
            End If

            If strAdminName.Trim.Length = 0 Then
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAdminName = strAdminName.Replace("&", "and")
                strAdminName = strAdminName.Replace("'", "")
                strAdminName = "<Property name=" & Chr(34) & "ShName" & Chr(34) & ">" & strAdminName & "</Property>"
            End If

            If strAddress1.Trim.Length = 0 Then
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress1 = strAddress1.Replace("&", "and")
                strAddress1 = strAddress1.Replace("'", "")
                strAddress1 = "<Property name=" & Chr(34) & "Address1" & Chr(34) & ">" & strAddress1 & "</Property>"
            End If

            If strAddress2.Trim.Length = 0 Then
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress2 = strAddress2.Replace("&", "and")
                strAddress2 = strAddress2.Replace("'", "")
                strAddress2 = "<Property name=" & Chr(34) & "Address2" & Chr(34) & ">" & strAddress2 & "</Property>"
            End If

            If strAddress3.Trim.Length = 0 Then
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strAddress3 = strAddress3.Replace("&", "and")
                strAddress3 = strAddress3.Replace("'", "")
                strAddress3 = "<Property name=" & Chr(34) & "City" & Chr(34) & ">" & strAddress3 & "</Property>"
            End If

            If strPostcode.Trim.Length = 0 Then
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & " nil=" & Chr(34) & "true" & Chr(34) & "/>"
            Else
                strPostcode = strPostcode.Replace("&", "and")
                strPostcode = strPostcode.Replace("'", "")
                strPostcode = "<Property name=" & Chr(34) & "PostCode" & Chr(34) & ">" & strPostcode & "</Property>"
            End If

            XMLString = "<Objects>"

            XMLString += "<Object DataType=" & Chr(34) & "Marel.Mp5.Base.Database.CompanyRecord" & Chr(34) & ">" &
                    "<Properties>" &
                    "<Property name=" & Chr(34) & "Code" & Chr(34) & ">" & "#" & dsSupplier.Tables(0).Rows(0).Item("Supplier") & "</Property>" &
                    "<Property name=" & Chr(34) & "Description5" & Chr(34) & ">" & strFlockNo & "</Property>" &
                    strName &
                    strAdminName &
                    strAddress1 &
                    strAddress2 &
                    strAddress3 &
                    strPostcode &
                    "</Properties></Object></Objects>"

            strSQL = "INSERT INTO itgr_imports (regTime,importStatus,ImportHandlerCode,Data)  VALUES(getdate(),0,'igih0010','" & XMLString & "')"

            executeInnovaNonQuery(strSQL)

        Catch ex As Exception
            MsgBox("Error creating supplier into Innova. Contact IT Department ." & vbCrLf & ex.Message)
        End Try


    End Sub
    Public Sub executeInnovaNonQuery(ByVal strSQL As String)

        Dim SQLCommand As SqlCommand

        Try

            SQLCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("PrimalInnova").ToString))
            SQLCommand.CommandTimeout = 140
            SQLCommand.Connection.Open()
            SQLCommand.ExecuteNonQuery()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()

        Catch objError As System.Data.SqlClient.SqlException
            '   MessageBox.Show(objError.Message)
        Catch
            '   MessageBox.Show(Err.Description)
        End Try

    End Sub

End Class
