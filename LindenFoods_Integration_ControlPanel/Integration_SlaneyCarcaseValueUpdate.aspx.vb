﻿Imports System.Data.SqlClient

Partial Class Integration_SlaneyCarcaseValueUpdate
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' IsAuthenticated("Linden", "cangui.yang", "J.boy.12-289")
        '  MsgBox(GetGroups())
        ' MsgBox(My.User.Name)
        ' GetGroupsList(My.User.Name)
    End Sub
    Protected Sub Button_Submit_Click(sender As Object, e As EventArgs) Handles Button_Submit.Click
        Try



            For Each row As GridViewRow In GridViewMatrix.Rows
                Dim txU As TextBox = row.FindControl("TextBox_U")
                Dim txR As TextBox = row.FindControl("TextBox_R")
                Dim txO As TextBox = row.FindControl("TextBox_O")
                Dim MaxtrixID As Integer = GridViewMatrix.DataKeys(row.RowIndex).Value
                Dim SQLInsertStr As String = "Insert into [tbl_SlaneyCarcaseValue] ([MatrixId],[DateRangeFrom],[DateRangeTo],[U],[R],[O]) Values " &
                    "(" & MaxtrixID & ",'" & TextBox_DateFrom.Text & "','" & TextBox_DateTo.Text & "',isnull(" & txU.Text & ",0),isnull(" & txR.Text & ",0),isnull(" & txO.Text & ",0))"
                ITWeb_executeNonQuery(SQLInsertStr)
            Next
            SqlDataSource_LastSetRecords.DataBind()
            GridView_CurrentMatrix.DataBind()
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(TryCast(sender, Control), Me.GetType(), "alert", ex.Message, True)
        End Try
    End Sub
    Protected Sub Button_Delete_Click(sender As Object, e As EventArgs) Handles Button_Delete.Click
        For Each row As GridViewRow In GridView_CurrentMatrix.Rows
            '"MatrixId,DateRangeFrom,DateRangeTo"
            Dim MaxtrixID As Integer = GridView_CurrentMatrix.DataKeys(row.RowIndex).Values(0)
            Dim DateRangeFrom As String = CDate(GridView_CurrentMatrix.DataKeys(row.RowIndex).Values(1)).ToString("yyyy-MM-dd")
            Dim DateRangeTo As String = CDate(GridView_CurrentMatrix.DataKeys(row.RowIndex).Values(2)).ToString("yyyy-MM-dd")
            '  MsgBox(DateRangeFrom)
            Dim SQLInsertStr As String = "Delete from tbl_SlaneyCarcaseValue where MatrixId=" & MaxtrixID & " and DateRangeFrom='" & DateRangeFrom & "' and DateRangeTo='" & DateRangeTo & "'"

            ITWeb_executeNonQuery(SQLInsertStr)
        Next
        GridView_CurrentMatrix.DataBind()
    End Sub
    Protected Sub Button_Apply_Click(sender As Object, e As EventArgs) Handles Button_Apply.Click
        '        Select Case proc_lots.lot, proc_lots.code, proc_individuals.mspencer, proc_individuals.stockvalue, proc_individuals.tesco, proc_individuals.osi, proc_individuals.tescofinest, proc_individuals.tescovalue, 
        '                         proc_individuals.hereford, proc_individuals.booker, proc_breeds.code As BreedCode, proc_breeds.name, Left(proc_yieldgroups.code, 1) As Yield, proc_items.id, proc_items.number
        'From proc_lots INNER Join
        '              proc_individuals On proc_lots.lot = proc_individuals.lot INNER Join
        '              proc_yieldgroups On proc_individuals.yieldgroup = proc_yieldgroups.yieldgroup INNER Join
        '              proc_items On proc_individuals.id = proc_items.individual LEFT OUTER Join
        '              proc_breeds On proc_individuals.breed = proc_breeds.breed
        'Where (proc_lots.name Like '%Slaney Foods%') AND (CONVERT(varchar(10), proc_lots.created, 120) = '2017-10-27')
        If GridView_CurrentMatrix.Rows.Count > 0 Then
            Dim DateRangeFrom As String = CDate(GridView_CurrentMatrix.DataKeys(0).Values(1)).ToString("yyyy-MM-dd")
            Dim DateRangeTo As String = CDate(GridView_CurrentMatrix.DataKeys(0).Values(2)).ToString("yyyy-MM-dd")

            ITWeb_executeNonQuery("Exec sp_SlaneyCarcaseValueMatrix @CarcaseRegDateFrom='" & DateRangeFrom & "',@CarcaseRegDateTo='" & DateRangeTo & "'")
            Dim AlertMessage = "alert('Update Qtr from " & GridView_CurrentMatrix.DataKeys(0).Values(1) & " to " & GridView_CurrentMatrix.DataKeys(0).Values(2) & " Completed');"
            ScriptManager.RegisterClientScriptBlock(TryCast(sender, Control), Me.GetType(), "alert", AlertMessage, True)
        End If


    End Sub

    Public Shared Function GetTime(ByVal context As HttpContext) _
            As String
        Return DateTime.Now.ToString()
    End Function


End Class
