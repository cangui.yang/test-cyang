﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Module DataConnection

    Dim timeOut As Int32 = 2000

    Public Function executeNonQuery(ByVal strSQL As String, ByVal connectionstr As String) As String
        Dim SQLCommand As SqlCommand

        Try

            SQLCommand = New SqlCommand(strSQL, New SqlConnection(connectionstr))
            SQLCommand.CommandTimeout = timeOut
            SQLCommand.Connection.Open()
            SQLCommand.ExecuteNonQuery()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()
            Return "0"
        Catch objError As System.Data.SqlClient.SqlException
            Return objError.Message
            ' MsgBox(objError.Message)
            ' EmailRetailSupport(objError.Message & vbCrLf & vbCrLf & strSQL)

        Catch
            Return Err.Description
            'MsgBox(Err.Description)
            ' EmailRetailSupport(Err.Description & vbCrLf & vbCrLf & strSQL)

        End Try
    End Function
    Public Sub ITWeb_executeNonQuery(ByVal strSQL As String)

        Dim SQLCommand As SqlCommand

        Try

            SQLCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("LindenITWebConnectionString").ConnectionString))
            SQLCommand.CommandTimeout = timeOut
            SQLCommand.Connection.Open()
            SQLCommand.ExecuteNonQuery()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()

        Catch objError As System.Data.SqlClient.SqlException
            MsgBox(objError.Message)
            ' EmailRetailSupport(objError.Message & vbCrLf & vbCrLf & strSQL)

        Catch
            MsgBox(Err.Description)
            ' EmailRetailSupport(Err.Description & vbCrLf & vbCrLf & strSQL)

        End Try

    End Sub
    Public Sub LindenBridge_executeNonQuery(ByVal strSQL As String)

        Dim SQLCommand As SqlCommand

        Try

            SQLCommand = New SqlCommand(strSQL, New SqlConnection(ConfigurationManager.ConnectionStrings("LindenBridgeConnectionString").ConnectionString))
            SQLCommand.CommandTimeout = timeOut
            SQLCommand.Connection.Open()
            SQLCommand.ExecuteNonQuery()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()

        Catch objError As System.Data.SqlClient.SqlException
            MsgBox(objError.Message)
            ' EmailRetailSupport(objError.Message & vbCrLf & vbCrLf & strSQL)

        Catch
            MsgBox(Err.Description)
            ' EmailRetailSupport(Err.Description & vbCrLf & vbCrLf & strSQL)

        End Try

    End Sub

    Public Function getScalarStr(ByVal strSQL As String, ByVal connectionstr As String) As String

        Dim strResult As String
        Dim objResult As Object
        Dim SQLCommand As SqlCommand

        Try
            SQLCommand = New SqlCommand(strSQL, New SqlConnection(connectionstr))
            '   selectCommand = New SqlClient.SqlCommand(strSQL, New SqlClient.SqlConnection(getInnovaIntegrationConnection()))

            SQLCommand.Connection.Open()
            objResult = SQLCommand.ExecuteScalar()
            SQLCommand.Connection.Close()
            SQLCommand.Dispose()

            If IsDBNull(objResult) Then
                strResult = 0
            Else
                strResult = objResult
            End If

        Catch objError As System.Data.SqlClient.SqlException
            ' MessageBox.Show(objError.Message)
            MsgBox(objError.Message)
        Catch
            MsgBox(Err.Description)


        End Try
        Return strResult

    End Function
End Module
