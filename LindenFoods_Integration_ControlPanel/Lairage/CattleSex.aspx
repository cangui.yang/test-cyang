﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Lairage/LairageSite.master" AutoEventWireup="false" CodeFile="CattleSex.aspx.vb" Inherits="Lairage_CattleSex" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
           var controlIDAcceptValue;
                                    function openKeyBoard(evt, Name) {
                                      var i, x, tablinks;
                                      x = document.getElementsByClassName("KeyBoard");
                                      for (i = 0; i < x.length; i++) {
                                         x[i].style.display = "none";
                                      }
                                      tablinks = document.getElementsByClassName("tablink");
                                      for (i = 0; i < x.length; i++) {
                                         tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
                                      }
                                      document.getElementById(Name).style.display = "block";
                                      evt.currentTarget.firstElementChild.className += " w3-border-red";
                                    }

                                    function openInformationPanel(evt,panel ) {
                                        var i, x, tablinks;
                                        x = document.getElementsByClassName("informationPanel");
                                        for (i = 0; i < x.length; i++) {
                                            x[i].style.display = "none";
                                        }

                                        document.getElementById(panel).style.display = "block";
                   
                                    }
                                    function KeyPaysetValue(val) {

                                        try {

                                            document.getElementById(controlIDAcceptValue).value = document.getElementById(controlIDAcceptValue).value + val;
                                            document.getElementById(controlIDAcceptValue).focus();
                                        }
                                        catch (err) {
                                            $('#myModal_AlertMessage').show();
                                            //   alert("Please set focus on text box to accept key pad value.");
                                           // document.getElementById('<'%=Label_AlertMessage.ClientID %>').innerText = " Please set focus on text box to accept key pad value."
                                        }

                                    }
                                    function setFocusID(id) {
                                        controlIDAcceptValue = id;
                                    }
                                    function ketPayDelete() {
                                        var currentValue = document.getElementById(controlIDAcceptValue).value;
                                        currentValue = currentValue.substring(0, currentValue.length - 1);
                                        document.getElementById(controlIDAcceptValue).value = currentValue;
                                    }
                                
        </script>

      <style>
    .KeyBoard {display:none;}
        .KeyBoardStyle {
              width:65px;
              height:52px;
              margin-bottom:5px;
              font-size:large;
        }
        .KeyBoardStyle_Alphabet {
              width:50px;
              height:45px;
              font-size:large;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
          <div class="container" style="width:980px; height:500px">
        <div class="row">
            <div class="col-md-8">
                <div id="NewSupplier" class="informationPanel" style="display:block">
                   <asp:UpdatePanel ID="UpdatePanel_SupplierDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>             
                      <table class="table">
                          <tr>
                              <td>Last 7 digits of Eartag:</td>
                              <td class="form-inline"><asp:TextBox ID="TextBox_Eartag_part" runat="server"  CssClass="form-control" AutoPostBack="false" onClick="setFocusID(this.id)" ></asp:TextBox>

                                  <ajaxToolkit:AutoCompleteExtender ID="TextBox_Eartag_part_AutoCompleteExtender" runat="server" BehaviorID="TextBox_Eartag_part_AutoCompleteExtender" DelimiterCharacters="" CompletionSetCount="10" CompletionInterval="1000" ServiceMethod="SearchProducts"  TargetControlID="TextBox_Eartag_part">
                                  </ajaxToolkit:AutoCompleteExtender>
                                 
                              
                            
                              </td>
                          </tr>
                      </table>
                          
                    </ContentTemplate>
                </asp:UpdatePanel>
                   
                </div>
              
                

            </div>
            <div class="col-md-4">
                 <asp:UpdatePanel ID="UpdatePanel_Keyboard" runat="server" UpdateMode="Always">
                           <ContentTemplate>
                            <div class="w3-row">
                              <a href="#" onclick="openKeyBoard(event, 'NUMBER');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red">NUMBER</div>
                              </a>
                              <a href="#" onclick="openKeyBoard(event, 'ALPHABET');">
                                <div class="w3-half tablink w3-bottombar w3-hover-light-grey w3-padding">ALPHABET</div>
                              </a>                         
                            </div>

                            <div id="NUMBER" class="w3-container KeyBoard" style="display:block; height:360px">
                                <div class="row" style="margin-top:10px">
                                    <div class="col-sm-4"><asp:Button ID="Button7" runat="server" Text="7" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button8" runat="server" Text="8" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button9" runat="server" Text="9" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button4" runat="server" Text="4" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button5" runat="server" Text="5" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button6" runat="server" Text="6" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button1" runat="server" Text="1" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button2" runat="server" Text="2" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button3" runat="server" Text="3" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button11" runat="server" Text="0" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button12" runat="server" Text="F" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button13" runat="server" Text="DEL" class="btn btn-info KeyBoardStyle" OnClientClick="ketPayDelete();return false;" /></div>
                                </div>
                                   <div class="row">
                                    <div class="col-sm-4"><asp:Button ID="Button14" runat="server" Text="." class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button15" runat="server" Text="-" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-4"><asp:Button ID="Button16" runat="server" Text="@" class="btn btn-info KeyBoardStyle" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <%--<div class="col-sm-12"><asp:Button ID="Button_Enter" ClientIDMode="Static" runat="server" Text="ENTER" class="btn btn-info KeyBoardStyle" Width="245px" /></div> --%>                                 
                                </div>
                            </div>

                            <div id="ALPHABET" class="w3-container KeyBoard" style="height:360px">
                                <div class="row" style="margin-bottom:5px; margin-top:10px">                                
                                    <div class="col-sm-3"><asp:Button ID="Button19" runat="server" Text="A" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button21" runat="server" Text="B" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button22" runat="server" Text="C" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button23" runat="server" Text="D" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                       
                                </div>
                                <div class="row" style="margin-bottom:6px">                                 
                                    <div class="col-sm-3"><asp:Button ID="Button24" runat="server" Text="E" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button25" runat="server" Text="F" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button26" runat="server" Text="G" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button27" runat="server" Text="H" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>           
                                </div>
                                <div class="row" style="margin-bottom:6px">                               
                                    <div class="col-sm-3"><asp:Button ID="Button28" runat="server" Text="I" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button29" runat="server" Text="J" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button30" runat="server" Text="K" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button31" runat="server" Text="L" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                      
                                </div>
                                <div class="row" style="margin-bottom:6px">                                 
                                    <div class="col-sm-3"><asp:Button ID="Button32" runat="server" Text="M" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button33" runat="server" Text="N" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button34" runat="server" Text="O" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button35" runat="server" Text="P" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>                                   
                                </div>
                                <div class="row" style="margin-bottom:6px">                                  
                                    <div class="col-sm-3"><asp:Button ID="Button36" runat="server" Text="Q" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button37" runat="server" Text="R" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button38" runat="server" Text="S" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button39" runat="server" Text="T" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>   
                                </div>
                                <div class="row" style="margin-bottom:6px">                                  
                                    <div class="col-sm-3"><asp:Button ID="Button40" runat="server" Text="U" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button41" runat="server" Text="V" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button42" runat="server" Text="W" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button43" runat="server" Text="X" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                </div>
                                <div class="row" style="margin-bottom:10px">     
                                    <div class="col-sm-3"><asp:Button ID="Button44" runat="server" Text="Y" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button45" runat="server" Text="Z" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button46" runat="server" Text=" " class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="KeyPaysetValue(this.value);return false;" /></div>
                                    <div class="col-sm-3"><asp:Button ID="Button47" runat="server" Text="Del" class="btn btn-info KeyBoardStyle_Alphabet" OnClientClick="ketPayDelete();return false;" /></div>
                                </div>

                            </div>
                           <%-- <table class="w3-table" style="text-align:center; margin-top:5px">
                                <tr>
                                    <td><asp:Button ID="Button_Verify" runat="server" Text="Farm" CssClass="btn btn-success"  Width="120px" Height="50px" /></td>
                                 
                                </tr>
                            </table>      --%>
                               
                           </ContentTemplate>
                       </asp:UpdatePanel>           
            </div>
        </div>
    </div>


      <div class="modal fade" id="myModal_AlertMessage" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alert Message</h4>            
        </div>
        <div class="modal-body" >
            <asp:UpdatePanel ID="UpdatePanel_Alert" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                   <div class="alert-info">
                        <strong>Warning! </strong><asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>        
        </div>
        <div class="modal-footer" style="padding-top:5px;"></div>
      </div>
    </div>
  </div>
</asp:Content>

