﻿Imports System.Data.SqlClient

Partial Class Lairage_CattleSex
    Inherits System.Web.UI.Page

    <Script.Services.ScriptMethod(), Services.WebMethod()>
    Public Shared Function SearchProducts(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim EarTag As List(Of String) = New List(Of String)
        Using cmd As SqlCommand = New SqlCommand("Select top 10 eartag from Killtrans where TransDate>getdate()-2 and replace(eartag,' ','') like '%" & prefixText.Replace("'", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("PrimalAmpsBeefConnectionString").ToString))
            cmd.Connection.Open()
            Using reader As SqlDataReader = cmd.ExecuteReader
                While reader.Read
                    EarTag.Add(reader("eartag").ToString)
                End While
            End Using

            cmd.Connection.Close()
            cmd.Dispose()
        End Using
        Return EarTag

    End Function

    ' Dim runQuery As New SQLAccess
    'Dim newSupplierID As Integer = 0
    '<System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    'Public Shared Function SearchCustomers(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
    '    Dim customers As List(Of String) = New List(Of String)
    '    Using cmd As SqlCommand = New SqlCommand("Select distinct top 50 Name from SuppLier where Name Like '" & prefixText.Replace("'", "") & "%'", New SqlConnection(ConfigurationManager.ConnectionStrings("AMPSLindenLambConnectionString").ToString))
    '        cmd.Connection.Open()
    '        Using reader As SqlDataReader = cmd.ExecuteReader
    '            While reader.Read
    '                customers.Add(reader("Name").ToString)
    '            End While
    '        End Using

    '        cmd.Connection.Close()
    '        cmd.Dispose()
    '    End Using
    '    Return customers

    'End Function

    'Protected Sub TextBox_Modal_SupplierKeyword_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Modal_SupplierKeyword.TextChanged
    '    SqlDataSource_Suppliers.DataBind()
    '    GridView_Modal_Suppliers.DataBind()
    'End Sub
    'Protected Sub GridView_Modal_Suppliers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView_Modal_Suppliers.SelectedIndexChanged

    '    If GridView_Modal_Suppliers.SelectedIndex > -1 Then
    '        '  Dim SupplierID As String = GridView_Modal_Suppliers.SelectedDataKey.ToString
    '        If FormView_Supplier.CurrentMode = FormViewMode.Insert Then

    '            FormView_Supplier.ChangeMode(FormViewMode.Edit)

    '        End If

    '        'FormView_Supplier.CurrentMode = FormViewMode.Edit
    '        UpdatePanel_SupplierDetails.Update()
    '        ScriptManager.RegisterStartupScript(GridView_Modal_Suppliers, GetType(Page), "showpanel", "openInformationPanel(event,'NewSupplier');", True)
    '    End If
    'End Sub
    'Protected Sub GridView_Modal_Suppliers_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GridView_Modal_Suppliers.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes("onclick") = Me.Page.ClientScript.GetPostBackClientHyperlink(GridView_Modal_Suppliers, "Select$" & e.Row.RowIndex)
    '    End If
    'End Sub

    'Protected Sub FormView_Supplier_ItemInserting(sender As Object, e As FormViewInsertEventArgs) Handles FormView_Supplier.ItemInserting
    '    newSupplierID = runQuery.getScalarInt("select max([Supplier])+1 from SuppLier")
    '    e.Values("Supplier") = newSupplierID

    'End Sub
    'Sub ShowAlertMessage(ByVal Msg As String)
    '    Label_AlertMessage.Text = Msg
    '    UpdatePanel_Alert.Update()
    '    ScriptManager.RegisterStartupScript(FormView_Supplier, GetType(Page), "showMyModal", "ShowModal('#myModal_AlertMessage');", True)
    'End Sub
End Class
