﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Integration_Kettyle.aspx.vb" Inherits="Integration_Kettyle" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="container">
  <h2>Kettyle Integration</h2>
  <p><strong>Note:Source Location </strong>\\lf-vm-test01\Users\cangui.yang\Documents\Visual Studio 2015\Projects\LindenFoods_Integration\Integration_Innova_KettyleAndNavision</p>
          <div class="panel-group" id="accordion">
              <div class="panel panel-info">
                  <div class="panel-heading">
                      <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                          SSIS Integration - Ship Sale Orders from Innova to Nav</a></h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
            
            <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="width: 249px"><strong>Integration Services Catalogs:</strong></td>
                    <td class="warning" style="width: 25%">Integration Kettyle</td>
                    <td class="danger" rowspan="5">closed SO in Innova to trigger this Integration</td>
                    <td class="success" rowspan="5">Click &#39;Reset&#39; to remove errors and untick Processed box,<br />
                        Click &#39;Process&#39; button to trigger the import again.</td>
                </tr>
                <tr>
                    <td style="width: 249px"><strong>Integration Services Project:</strong></td>
                    <td class="warning" style="width: 25%">Integration_Kettyle_InnovaAndNavision</td>
                </tr>
                <tr>
                    <td style="width: 249px"><strong>Integration Services Packages:</strong></td>
                    <td class="warning" style="width: 25%">Kettyle Ship Sales Orders Innova2Nav.dtsx</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px;"><strong>SQL Agent Job:</strong></td>
                    <td style="height: 22px; width: 25%;" class="warning">Integration_Kettyle_SO_Innova2Nav</td>
                </tr>
              
                <tr>
                    <td style="width: 249px; height: 22px;"><strong>Flow Chart:</strong></td>
                    <td style="height: 22px; width: 25%;" class="warning">
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="\\10.0.3.15\it\Application Integration Design &amp; Doc\SO Kettyle Innova2Nav.vsd" Target="_blank">View Chart</asp:HyperLink>
                    </td>
                </tr>
              
            </table>
            <br />
             <asp:UpdatePanel ID="UpdatePanel_SSIS_SO" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" >        <%--Height="300px" ScrollBars="Vertical"--%>                        
                          <asp:GridView ID="GridView_SSIS_SO" class="table" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_SSISSOInnova2Nav" DataKeyNames="ID" AllowSorting="True" AllowPaging="True" PageSize="15">
                      <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton_SOReset" runat="server" OnClick="LinkButton_SOReset_Click" >Reset</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_SOReset_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton1_ConfirmButtonExtender" ConfirmText="Are you sure you want to reset this line?" TargetControlID="LinkButton_SOReset" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                        <asp:BoundField DataField="NavOrderNo" HeaderText="NavOrderNo" SortExpression="NavOrderNo" />
                        <asp:BoundField DataField="NavProduct" HeaderText="NavProduct" SortExpression="NavProduct" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="qtyToShip" HeaderText="qtyToShip" SortExpression="qtyToShip" />
                        <asp:BoundField DataField="ConsigneQty" HeaderText="ConsigneQty" SortExpression="ConsigneQty" />
                        <asp:BoundField DataField="LastUpdatedDatetime" HeaderText="LastUpdatedDatetime" SortExpression="LastUpdatedDatetime" />
                        <asp:CheckBoxField DataField="Processed" HeaderText="Processed" SortExpression="Processed" />
                        <asp:BoundField DataField="NavSaleUOM" HeaderText="NavSaleUOM" SortExpression="NavSaleUOM" />
                        <asp:BoundField DataField="Errors" HeaderText="Errors" SortExpression="Errors" />
                    </Columns> <EmptyDataRowStyle BackColor="#33CC33" />
                              <EmptyDataTemplate>
                                  There is no SO Close yet today.
                              </EmptyDataTemplate>
                </asp:GridView>

                <asp:SqlDataSource ID="SqlDataSource_SSISSOInnova2Nav" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" SelectCommand="SELECT [ID], [NavOrderNo], [NavProduct], [Quantity], [qtyToShip], [ConsigneQty], [LastUpdatedDatetime], [Processed], [NavSaleUOM], [Errors] FROM [tblSaleslog_Kettyle_Innova2Nav_withShipping] WHERE ([LastUpdatedDatetime] &gt;= Getdate()-5) ORDER BY [LastUpdatedDatetime] DESC, [NavOrderNo] DESC">
                  
                            </asp:SqlDataSource>
                               
                
            </asp:Panel>
                        <br />
                 <asp:Button ID="Button_SSIS_SO_Process" runat="server" Text="Process" class="btn btn-primary" /> 
                 <asp:Button ID="Button_SSIS_SO_Refresh" runat="server" Text="Refresh" class="btn btn-success" /> 
                  </ContentTemplate>
             </asp:UpdatePanel>
        </div>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
            SSIS Integration - Purchase Orders from Innova to Nav</a></h4>
      </div>
         <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
             <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="width: 249px; height: 22px;"><strong>Integration Services Catalogs:</strong></td>
                    <td style="height: 22px" class="warning">Integration Kettyle</td>
                    <td class="danger" rowspan="6">Close PO in Innova to triger the integration.<br />
                        Script ID=46 script Name= CY Integration Script on Close PO</td>
                    <td class="success" rowspan="6">Click &#39;Reset&#39; on the records<br />
                        Click &#39;Process&#39; button to trigger the update in Nav again.</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px;"><strong>Integration Services Project:</strong></td>
                    <td style="height: 22px" class="warning">Integration_Kettyle_InnovaAndNavision</td>
                </tr>
                <tr>
                    <td style="width: 249px"><strong>Integration Services Packages:</strong></td>
                    <td class="warning">Kettyle PurchaseOrders Innova2Nav.dtsx</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 23px;"><strong>SQL Agent Job:</strong></td>
                    <td style="height: 23px" class="warning">Integration_Kettyle_PO_Innova2Nav</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 23px;"><strong>Log table:</strong></td>
                    <td style="height: 23px" class="warning">[InnovaIntegration].[dbo].[tbl_SSISIntegration_Log]</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 23px;"><strong>Flow Chart</strong></td>
                    <td style="height: 23px" class="warning">&nbsp;</td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UpdatePanel_SSIS_PO" runat="server">
                <ContentTemplate>
                       <asp:GridView ID="GridView_SSIS_PO" CssClass="table" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_SSISPOInnova2Nav" AllowSorting="True">
                 <Columns>
                     <asp:TemplateField>
                         <ItemTemplate>
                             <asp:LinkButton ID="LinkButton_SSIS_PO" runat="server" OnClick="LinkButton_SSIS_PO_Click">Reset </asp:LinkButton>
                             <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_SSIS_PO_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_SSIS_PO_ConfirmButtonExtender" ConfirmText="Are you sure you want to Reset this record?" TargetControlID="LinkButton_SSIS_PO" />
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                     <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value" />
                     <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                     <asp:BoundField DataField="DateTimeInserted" HeaderText="DateTimeInserted" SortExpression="DateTimeInserted" />
                     <asp:BoundField DataField="Processed" HeaderText="Processed" SortExpression="Processed" />
                     <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                     <asp:BoundField DataField="DateTimeProcessed" HeaderText="DateTimeProcessed" SortExpression="DateTimeProcessed" />
                 </Columns>
                 <EmptyDataRowStyle BackColor="#33CC33" />
                 <EmptyDataTemplate>
                     There is no PO closed yet today.
                 </EmptyDataTemplate>
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource_SSISPOInnova2Nav" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" DeleteCommand="DELETE FROM [tbl_SSISIntegration_Log] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tbl_SSISIntegration_Log] ([Value], [Type], [DateTimeInserted], [Processed], [ErrorMessage], [DateTimeProcessed]) VALUES (@Value, @Type, @DateTimeInserted, @Processed, @ErrorMessage, @DateTimeProcessed)" SelectCommand="SELECT * FROM [tbl_SSISIntegration_Log] where [DateTimeInserted] >getdate()-1 ORDER BY [DateTimeInserted] DESC, [Processed]" UpdateCommand="UPDATE [tbl_SSISIntegration_Log] SET [Value] = @Value, [Type] = @Type, [DateTimeInserted] = @DateTimeInserted, [Processed] = @Processed, [ErrorMessage] = @ErrorMessage, [DateTimeProcessed] = @DateTimeProcessed WHERE [Id] = @Id">
                 <DeleteParameters>
                     <asp:Parameter Name="Id" Type="Int32" />
                 </DeleteParameters>
                 <InsertParameters>
                     <asp:Parameter Name="Value" Type="String" />
                     <asp:Parameter Name="Type" Type="String" />
                     <asp:Parameter Name="DateTimeInserted" Type="DateTime" />
                     <asp:Parameter Name="Processed" Type="String" />
                     <asp:Parameter Name="ErrorMessage" Type="String" />
                     <asp:Parameter Name="DateTimeProcessed" Type="DateTime" />
                 </InsertParameters>
                 <UpdateParameters>
                     <asp:Parameter Name="Value" Type="String" />
                     <asp:Parameter Name="Type" Type="String" />
                     <asp:Parameter Name="DateTimeInserted" Type="DateTime" />
                     <asp:Parameter Name="Processed" Type="String" />
                     <asp:Parameter Name="ErrorMessage" Type="String" />
                     <asp:Parameter Name="DateTimeProcessed" Type="DateTime" />
                     <asp:Parameter Name="Id" Type="Int32" />
                 </UpdateParameters>
             </asp:SqlDataSource>
             <br />
                     <asp:Button ID="Button_SSIS_PO" runat="server" Text="Process" class="btn btn-primary" />
                    <asp:Button ID="Button_SSIS_PO_Refresh" runat="server" Text="Refresh" class="btn btn-success" /> 
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
      </div>
    </div>

    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
            SP Integration - Transfer Orders from Nav to Innova</a></h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">

            <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning" >KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger" ><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success" ><strong>How to</strong></td>
                </tr>
                <tr >
                    <td ><strong>Database:</strong></td>
                    <td class="warning">Innova</td>
                    <td  class="danger" rowspan="4">&nbsp;</td>
                    <td  class="success" rowspan="4">Open and Release the TF order to tirgger the integration again</td>
                </tr>
                <tr>
                    <td ><strong>Integration Stored Procedures:</strong></td>
                    <td class="warning">sp_Integration_TF_Nav_Kettyle2Innova</td>
                </tr>
                <tr>
                    <td ><strong>Triggered by Nav:</strong></td>
                    <td class="warning">Trasfer Order Status::Release</td>
                </tr>
                <tr>
                    <td ><strong>Flow Chart</strong></td>
                    <td class="warning">&nbsp;</td>
                </tr>
              
            </table>
            <br /><asp:UpdatePanel ID="UpdatePanel_SP_TF" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            
                        <asp:GridView ID="GridView_SP_TF" CssClass="table" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource_SP_TF" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton_TF_Delete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="LinkButton_TF_Delete_ConfirmButtonExtender" runat="server" BehaviorID="LinkButton_TF_Delete_ConfirmButtonExtender" ConfirmText="Are you sure you want to delete this Log record?" TargetControlID="LinkButton_TF_Delete" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                                <asp:BoundField DataField="TransferOrder" HeaderText="TransferOrder" SortExpression="TransferOrder" />
                                <asp:BoundField DataField="Nav_timestamp" HeaderText="Nav_timestamp" SortExpression="Nav_timestamp" />
                                <asp:BoundField DataField="Nav_Datestamp" HeaderText="Nav_Datestamp" SortExpression="Nav_Datestamp" />
                                <asp:BoundField DataField="ImportedTime" HeaderText="ImportedTime" SortExpression="ImportedTime" />
                            </Columns>
                            <EmptyDataRowStyle BackColor="#009900" />
                            <EmptyDataTemplate>
                                There is no transfer order yet today
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SP_TF" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" DeleteCommand="DELETE FROM [tbl_TransfeOrder_TRK] WHERE [ID] = @ID" SelectCommand="SELECT * FROM [tbl_TransfeOrder_TRK] WHERE ImportedTime>=Getdate()-5 ORDER BY [ImportedTime] DESC, [TransferOrder] DESC">
                            <DeleteParameters>
                                <asp:Parameter Name="ID" Type="Int32" />
                            </DeleteParameters>                                                   
                        </asp:SqlDataSource>
                                <asp:Button ID="Button_SP_TF_Refresh" runat="server" Text="Refresh" class="btn btn-success" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
        </div>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
            SP Integration - Sales order from Nav to Innova</a></h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse in ">
        <div class="panel-body">

            <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td><strong>Database:</strong></td>
                    <td class="warning">Innova</td>
                    <td class="danger" rowspan="4">&nbsp;</td>
                    <td class="success" rowspan="4">Open and realease the SO to trigger the integration again</td>
                </tr>
                <tr>
                    <td><strong>Integration Stored Procedures:</strong></td>
                    <td class="warning">sp_Integration_SO_Nav_Kettyle_LB2Innova</td>
                </tr>
                <tr>
                    <td><strong>Triggered by Nav:</strong></td>
                    <td class="warning">Sales Order Status::Release</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="warning">
                        
                    </td>
                </tr>
              
            </table>
            <br /><asp:UpdatePanel ID="UpdatePanel_SP_SO" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
<%--                        <asp:GridView ID="GridView_SP_SO" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="SalesOrderNo" DataSourceID="SqlDataSource_SP_SO" PageSize="15">
                            <Columns>
                                <asp:BoundField DataField="SalesOrderNo" HeaderText="SalesOrderNo" ReadOnly="True" SortExpression="SalesOrderNo" />
                                <asp:BoundField DataField="ImportTime" HeaderText="ImportTime" SortExpression="ImportTime" />
                                <asp:BoundField DataField="Nav_timestamp" HeaderText="Nav_timestamp" SortExpression="Nav_timestamp" />
                                <asp:BoundField DataField="Nav_Datestamp" HeaderText="Nav_Datestamp" SortExpression="Nav_Datestamp" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SP_SO" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" DeleteCommand="DELETE FROM [SalesOrder_Transfered] WHERE [SalesOrderNo] = @SalesOrderNo" InsertCommand="INSERT INTO [SalesOrder_Transfered] ([SalesOrderNo], [ImportTime], [Nav_timestamp], [Nav_Datestamp]) VALUES (@SalesOrderNo, @ImportTime, @Nav_timestamp, @Nav_Datestamp)" SelectCommand="SELECT SalesOrderNo, ImportTime, Nav_timestamp, Nav_Datestamp FROM SalesOrder_Transfered WHERE (ImportTime &gt; GETDATE() - 5) ORDER BY ImportTime DESC, SalesOrderNo DESC" UpdateCommand="UPDATE [SalesOrder_Transfered] SET [ImportTime] = @ImportTime, [Nav_timestamp] = @Nav_timestamp, [Nav_Datestamp] = @Nav_Datestamp WHERE [SalesOrderNo] = @SalesOrderNo">
                            <DeleteParameters>
                                <asp:Parameter Name="SalesOrderNo" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="SalesOrderNo" Type="String" />
                                <asp:Parameter Name="ImportTime" Type="DateTime" />
                                <asp:Parameter Name="Nav_timestamp" Type="DateTime" />
                                <asp:Parameter Name="Nav_Datestamp" Type="DateTime" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="ImportTime" Type="DateTime" />
                                <asp:Parameter Name="Nav_timestamp" Type="DateTime" />
                                <asp:Parameter Name="Nav_Datestamp" Type="DateTime" />
                                <asp:Parameter Name="SalesOrderNo" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>--%>
                                <asp:GridView ID="GridView_SP_SO_Header" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_SP_SOHeader">
                                    <Columns>
                                        <asp:BoundField DataField="SalesOrderNo" HeaderText="SalesOrderNo" SortExpression="SalesOrderNo" />
                                        <asp:TemplateField HeaderText="ImportType" SortExpression="ImportType">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ImportType").ToString.Replace(1, "Insert").Replace(2, "Update") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ItemLastDateModified" DataFormatString="{0:dd/MM/yyyy}" HeaderText="DateModified" SortExpression="ItemLastDateModified" />
                                        <asp:BoundField DataField="ItemLastTimeModified" DataFormatString="{0:HH:mm:ss}" HeaderText="TimeModified" SortExpression="ItemLastTimeModified" />
                                        <asp:BoundField DataField="DateTimeProcess" HeaderText="DateTimeProcess" SortExpression="DateTimeProcess" />
                                        <asp:BoundField DataField="DateTimeImport" HeaderText="DateTimeImport" SortExpression="DateTimeImport" />
                                        <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource_SP_SOHeader" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" DeleteCommand="DELETE FROM [tblIntegration_SO_Nav_Kettyle2Innova_Kettyle_Orders] WHERE [Id] = @Id" InsertCommand="INSERT INTO [tblIntegration_SO_Nav_Kettyle2Innova_Kettyle_Orders] ([SalesOrderNo], [SellToCustomerNo], [DepotCustomer], [OrderDesc], [OrderDate], [DespatchDate], [DeliveryDate], [ItemLastDateModified], [ItemLastTimeModified], [ImportType], [ExternalDocumentNo], [DaysToDeliver], [InnovaDespReport], [Status], [ErrorMessage], [DateTimeProcess], [DateTimeImport], [DespatchReportLayout], [OrderCompleted]) VALUES (@SalesOrderNo, @SellToCustomerNo, @DepotCustomer, @OrderDesc, @OrderDate, @DespatchDate, @DeliveryDate, @ItemLastDateModified, @ItemLastTimeModified, @ImportType, @ExternalDocumentNo, @DaysToDeliver, @InnovaDespReport, @Status, @ErrorMessage, @DateTimeProcess, @DateTimeImport, @DespatchReportLayout, @OrderCompleted)" SelectCommand="SELECT * FROM [tblIntegration_SO_Nav_Kettyle2Innova_Kettyle_Orders] ORDER BY [DateTimeImport] DESC, [SalesOrderNo] DESC" UpdateCommand="UPDATE [tblIntegration_SO_Nav_Kettyle2Innova_Kettyle_Orders] SET [SalesOrderNo] = @SalesOrderNo, [SellToCustomerNo] = @SellToCustomerNo, [DepotCustomer] = @DepotCustomer, [OrderDesc] = @OrderDesc, [OrderDate] = @OrderDate, [DespatchDate] = @DespatchDate, [DeliveryDate] = @DeliveryDate, [ItemLastDateModified] = @ItemLastDateModified, [ItemLastTimeModified] = @ItemLastTimeModified, [ImportType] = @ImportType, [ExternalDocumentNo] = @ExternalDocumentNo, [DaysToDeliver] = @DaysToDeliver, [InnovaDespReport] = @InnovaDespReport, [Status] = @Status, [ErrorMessage] = @ErrorMessage, [DateTimeProcess] = @DateTimeProcess, [DateTimeImport] = @DateTimeImport, [DespatchReportLayout] = @DespatchReportLayout, [OrderCompleted] = @OrderCompleted WHERE [Id] = @Id">
                 
                                </asp:SqlDataSource>
                <asp:Button ID="Button_SP_SO_Refresh" runat="server" Text="Refresh" class="btn btn-success" />
                            </ContentTemplate>
                       </asp:UpdatePanel>
          </div>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
            SP Integration - Purchase Order From Nav to Innova</a></h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse in">
        <div class="panel-body">

            <table class="table">
                <tr >
                    <td style="width: 25%; height: 37px;"><strong>Hosting Server:</strong></td>
                    <td style="width: 25%; height: 37px;" class="warning">KFSQL\INNOVA</td>
                    <td style="width: 25%; height: 37px;" class="danger"><strong>Advance info</strong></td>
                    <td style="width: 25%; height: 37px;" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="height: 37px" ><strong>Database:</strong></td>
                    <td class="warning" style="height: 37px" >Innova</td>
                    <td class="danger" rowspan="4" >&nbsp;</td>
                    <td class="success" rowspan="4" ></td>
                </tr>
                <tr>
                    <td ><strong>Integration Stored Procedures:</strong></td>
                    <td class="warning" >sp_Integration_PO_Nav_Kettyle_LB2Innova</td>
                </tr>
                <tr>
                    <td ><strong>Triggered by Nav:</strong></td>
                    <td class="warning" >Sales Order Status::Release</td>
                </tr>
                <tr>
                    <td ><strong>Flow chart</strong></td>
                    <td class="warning" >
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="\\10.0.3.15\it\Application Integration Design &amp; Doc\PO Kettyle Nav2Innova.vsdx" Target="_blank">View Chart</asp:HyperLink>
                    </td>
                </tr>
              
            </table>
            <br />
            <asp:UpdatePanel ID="UpdatePanel_SP_PO" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
<asp:GridView ID="GridView_SP_PO" runat="server" CssClass="table" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_SP_PO">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="PONo" HeaderText="PONo" SortExpression="PONo" />
                    <asp:BoundField DataField="BuyFromCustomerNo" HeaderText="BuyFromCustomerNo" SortExpression="BuyFromCustomerNo" />
                    <asp:BoundField DataField="SupplyCode" HeaderText="SupplyCode" SortExpression="SupplyCode" />
                    <asp:BoundField DataField="ImportType" HeaderText="ImportType" SortExpression="ImportType" />
                    <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                    <asp:BoundField DataField="Inventory" HeaderText="Inventory" SortExpression="Inventory" />
                    <asp:BoundField DataField="DateTimeImport" HeaderText="DateTimeImport" SortExpression="DateTimeImport" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource_SP_PO" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" SelectCommand="SELECT Id, PONo, BuyFromCustomerNo, SupplyCode, ImportType, ErrorMessage, Inventory, DateTimeImport FROM tblIntegration_PO_Nav_Kettyle2Innova_Kettyle_Orders WHERE (DateTimeImport &gt;= GETDATE() - 5) ORDER BY DateTimeImport DESC"></asp:SqlDataSource>
                    <asp:Button ID="Button_SP_PO_Refresh" runat="server" Text="Refresh" class="btn btn-success" />
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <br />
          </div>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
            SP Integration - Auto Fix</a></h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse in">
        <div class="panel-body">  <table class="table">
                <tr >
                    <td style="width: 249px"><strong>Hosting Server:</strong></td>
                    <td style="width: 251px" class="warning">KFSQL\INNOVA</td>
                    <td style="width: 251px" class="danger"><strong>Advance Info</strong></td>
                    <td style="width: 251px" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td style="width: 249px; height: 22px;"><strong>Database:</strong></td>
                    <td style="width: 251px; height: 22px" class="warning">LF-SQL01\UTILITY1.LindenBridge</td>
                    <td style="width: 251px; " class="danger" rowspan="4">&nbsp;</td>
                    <td style="width: 251px; " class="success" rowspan="4">&nbsp;</td>
                </tr>
                <tr >
                    <td style="width: 249px; height: 22px;"><strong>Table:</strong></td>
                    <td style="width: 251px; height: 22px" class="warning">LindenBridge.dbo.tbl_AutoFix_Setting</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px;"><strong>Integration Stored Procedures:</strong></td>
                    <td style="width: 251px; height: 22px;" class="warning">Innova.dbo.sp_Integration_Autofix</td>
                </tr>
                <tr>
                    <td style="width: 249px"><strong>SQL Server Agent:</strong></td>
                    <td style="width: 251px" class="warning">Integration_Kettyle_AutoFix</td>
                </tr>
            </table>
            <table>
                <tr>                
                    <td style="width:50%; vertical-align:top" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                        <asp:GridView ID="GridView1" CssClass="table" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Site,HandlerCode" DataSourceID="SqlDataSource_AutoFix">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" />
                                <asp:BoundField DataField="HandlerCode" HeaderText="HandlerCode" ReadOnly="True" SortExpression="HandlerCode" />
                                <asp:CheckBoxField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                                <asp:Button ID="Button_AutoFix" runat="server" class="btn btn-primary" Text="Process" />
                        <asp:SqlDataSource ID="SqlDataSource_AutoFix" runat="server" ConnectionString="<%$ ConnectionStrings:LindenBridgeConnectionString %>" DeleteCommand="DELETE FROM [tbl_AutoFix_Setting] WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode" InsertCommand="INSERT INTO [tbl_AutoFix_Setting] ([Site], [HandlerCode], [Status]) VALUES ('Kettyle', @HandlerCode, @Status)" SelectCommand="SELECT * FROM [tbl_AutoFix_Setting] WHERE ([Site] = @Site) ORDER BY [HandlerCode]" UpdateCommand="UPDATE [tbl_AutoFix_Setting] SET [Status] = @Status WHERE [Site] = @Site AND [HandlerCode] = @HandlerCode">
                            <DeleteParameters>
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="HandlerCode" Type="String" />
                            </DeleteParameters>
                            <InsertParameters>
                                <%--<asp:Parameter Name="Site" Type="String" />--%>
                                <asp:Parameter Name="HandlerCode" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="Kettyle" Name="Site" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Id" Type="Int32" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="HandlerCode" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="width:25%; vertical-align:top">
                        <asp:UpdatePanel ID="UpdatePanel_Itgr_Import" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <%--    <asp:Panel ID="Panel2" runat="server" Width="500px" ScrollBars="Auto" Height="300">--%>
                                    <asp:GridView ID="GridView_ITGr_Import" runat="server" CssClass="table" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource_ItgrImport_log">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton_ItgrImport_Reset" runat="server" OnClick="LinkButton_ItgrImport_Reset_Click">Reset</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                                            <asp:BoundField DataField="regtime" HeaderText="regtime" SortExpression="regtime" />
                                            <asp:BoundField DataField="importstatus" HeaderText="importstatus" SortExpression="importstatus" />
                                            <asp:BoundField DataField="importhandlercode" HeaderText="handlercode" SortExpression="importhandlercode" />
                                        </Columns>
                                        <EmptyDataRowStyle BackColor="#009900" />
                                        <EmptyDataTemplate>
                                            There is no Errors in ITGR_Import
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                              <%--  </asp:Panel>--%>
                                <asp:Label ID="Label_Itgr_Data" runat="server" Text=""></asp:Label><br />
                                <asp:Label ID="Label_Itgr_error" runat="server" Text=""></asp:Label>
                        <asp:SqlDataSource ID="SqlDataSource_ItgrImport_log" runat="server" ConnectionString="<%$ ConnectionStrings:KettyleInnovaConnectionString %>" SelectCommand="SELECT id, regtime, importstatus, importhandlercode, data FROM itgr_imports WHERE importstatus=3 and (CONVERT (varchar(10), regtime, 120) = CONVERT (varchar(10), GETDATE(), 120))"></asp:SqlDataSource>
                
                                <asp:Button ID="Button_Autofix_Refresh" runat="server" class="btn btn-success" Text="Refresh" />
                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                         </td>
                </tr>
              
            </table></div>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
            SP Integration - Get Packs & Lots from SI</a></h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse in">
        <div class="panel-body">  <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger"><strong>Advance Info</strong></td>
                    <td style="width:25%" class="success"><strong>How to</strong></td>
                </tr>
                <tr >
                    <td ><strong>Database:</strong></td>
                    <td class="warning" >KFSQL\INNOVA.dbo.InnovaIntegration</td>
                    <td class="danger" rowspan="5" >&nbsp;</td>
                    <td class="success" rowspan="5" >&nbsp;</td>
                </tr>
                <tr >
                    <td ><strong>Table:</strong></td>
                    <td class="warning" >LindenBridge.dbo.tbl_SI_ImportLog</td>
                </tr>
                <tr>
                    <td ><strong>Integration Stored Procedures:</strong></td>
                    <td class="warning" >Innova.dbo.sp_Get_LotsAndPacks_FromSI</td>
                </tr>
                <tr>
                    <td ><strong>SQL Server Agent:</strong></td>
                    <td class="warning" >Execute Transfer from SI to Kettyle Innova Pack records</td>
                </tr>
                <tr>
                    <td >&nbsp;</td>
                    <td class="warning"></td>
                </tr>
              
            </table>   <asp:UpdatePanel ID="UpdatePanel_SP_SI" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="GridView_SP_SI" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource_SP_SI">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                                        <asp:BoundField DataField="SIOrderNo" HeaderText="SIOrderNo" SortExpression="SIOrderNo" />
                                        <asp:BoundField DataField="ImportStatus" HeaderText="ImportStatus" SortExpression="ImportStatus" />
                                        <asp:BoundField DataField="NavOrderNo" HeaderText="NavOrderNo" SortExpression="NavOrderNo" />
                                        <asp:BoundField DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage" />
                                        <asp:BoundField DataField="ImportedDateTime" HeaderText="ImportedDateTime" SortExpression="ImportedDateTime" />
                                    </Columns>
                                </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource_SP_SI" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" SelectCommand="SELECT * FROM [tbl_SI_ImportLog] where ImportedDateTime>=GetDate()-3">
                        </asp:SqlDataSource>
                                <asp:Button ID="Button_SP_SI_Refresh" runat="server" class="btn btn-success" Text="Refresh" />
                            </ContentTemplate>
                        </asp:UpdatePanel></div>
      </div>
    </div>
      <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
            SP Integration - Get Packs From Retail</a></h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse in">
        <div class="panel-body">  <table class="table">
                <tr >
                    <td style="width:25%"><strong>Hosting Server:</strong></td>
                    <td style="width:25%" class="warning">KFSQL\INNOVA</td>
                    <td style="width:25%" class="danger">&nbsp;</td>
                    <td style="width:25%" class="success">&nbsp;</td>
                </tr>
                <tr >
                    <td ><strong>Database:</strong></td>
                    <td class="warning" >KFSQL\INNOVA</td>
                    <td class="danger" ></td>
                    <td class="success" >&nbsp;</td>
                </tr>
                <tr >
                    <td ><strong>Table:</strong></td>
                    <td class="warning" >LFSQL\INNOVA.InnovaIntegration.dbo.tbl_PacksFromRetails</td>
                    <td class="danger" >&nbsp;</td>
                    <td class="success" >&nbsp;</td>
                </tr>
                <tr>
                    <td ><strong>Integration Stored Procedures:</strong></td>
                    <td class="warning" >GetPacksFrom_RetailInnova_to_KettyleInnova_Batch</td>
                    <td class="danger" ></td>
                    <td class="success" >&nbsp;</td>
                </tr>
                <tr>
                    <td ><strong>SQL Server Agent:</strong></td>
                    <td class="warning" >Copy Packs From Retail Innova to Kettyle Innova</td>
                    <td class="danger" >&nbsp;</td>
                    <td class="success" >&nbsp;</td>
                </tr>
                <tr>
                    <td ><strong>Manual Import:</strong></td>
                    <td class="warning" >
                        <asp:TextBox ID="TextBox_RetailsSO" runat="server"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBox_RetailsSO_TextBoxWatermarkExtender" runat="server" BehaviorID="TextBox_RetailsSO_TextBoxWatermarkExtender" TargetControlID="TextBox_RetailsSO" WatermarkText="Enter Retail SO" />
&nbsp;<asp:Button ID="Button_PacksFromRetails" runat="server" Text="Process" class="btn btn-primary" />  
                                </td>
                    <td class="danger" >&nbsp;</td>
                    <td class="success" >&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 249px; height: 22px; vertical-align:top"><%--<asp:Button ID="Button_GetPackfromSI" runat="server" Text="Process" class="btn btn-primary" />--%>  
                    </td>
                    <td style="height: 22px; width: 251px; vertical-align:top" class="warning" >
                       
                    </td>
                    <td style="height: 22px; width: 251px; vertical-align:top" class="danger">
                        &nbsp;</td>
                    <td style="height: 22px; width: 251px; vertical-align:top" class="success">
                        &nbsp;</td>
                </tr>
              
            </table> <asp:UpdatePanel ID="UpdatePanel_GetPackFromRetail" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="GridView_PacksFromRetails" CssClass="table" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_SP_GetPacksFromRetail" PageSize="15">
                                    <Columns>
                                        <asp:BoundField DataField="BatchNo" HeaderText="BatchNo" SortExpression="BatchNo" />
                                        <asp:BoundField DataField="ImportedDateTime" HeaderText="ImportedDateTime" SortExpression="ImportedDateTime" />
                                        <asp:BoundField DataField="OrderNo" HeaderText="OrderNo" SortExpression="OrderNo" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource_SP_GetPacksFromRetail" runat="server" ConnectionString="<%$ ConnectionStrings:KFInnovaIntegrationConnectionString %>" SelectCommand="SELECT BatchNo, ImportedDateTime, OrderNo FROM tbl_PacksFromRetails WHERE (ImportedDateTime &gt;= GETDATE() - 2)">
                                </asp:SqlDataSource>
                                <asp:Button ID="Button_SP_GetPackLotFromSI_Refresh" runat="server" class="btn btn-success" Text="Refresh" />
                            </ContentTemplate>
                        </asp:UpdatePanel></div>
      </div>
    </div>
  </div> 
    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Alert Message:</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanel_AlertMessage" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                      <asp:Label ID="Label_AlertMessage" runat="server" Text=""></asp:Label>
                  </ContentTemplate>
              </asp:UpdatePanel>
              
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
</asp:Content>

