﻿Imports System.Data.SqlClient
Partial Class Integration_Kettyle
    Inherits System.Web.UI.Page

    Sub PopUpMessage(ByVal messagestr As String)
        Label_AlertMessage.Text = messagestr
        UpdatePanel_AlertMessage.Update()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
    End Sub
    Protected Sub LinkButton_SOReset_Click(sender As Object, e As EventArgs)
        'ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView_SSIS_SO.DataKeys(row.RowIndex).Value.ToString
        Dim SQLstr = "Update tblSaleslog_Kettyle_Innova2Nav_withShipping set Processed=0, Errors=null where ID=" & pk
        executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
        GridView_SSIS_SO.DataBind()
        UpdatePanel_SSIS_SO.Update()
    End Sub
    Protected Sub Button_SSIS_SO_Process_Click(sender As Object, e As EventArgs) Handles Button_SSIS_SO_Process.Click
        Try
            Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Integration_Kettyle_SO_Innova2Nav'"
            executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
            PopUpMessage("Process Completed")
        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub Button_SSIS_PO_Click(sender As Object, e As EventArgs) Handles Button_SSIS_PO.Click
        Try
            Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Integration_Kettyle_PO_Innova2Nav'"
            executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
            PopUpMessage("Process Completed")
        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub
    Protected Sub LinkButton_SSIS_PO_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView_SSIS_PO.DataKeys(row.RowIndex).Value.ToString
        Dim SQLstr = "Update [tbl_SSISIntegration_Log] set Processed='N', ErrorMessage=Null where Id=" & pk
        executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
        GridView_SSIS_PO.DataBind()
        UpdatePanel_SSIS_PO.Update()
    End Sub

    Protected Sub Button_AutoFix_Click(sender As Object, e As EventArgs) Handles Button_AutoFix.Click
        Try
            Dim SQLStr As String = "EXEC msdb.dbo.sp_start_job N'Integration_Kettyle_Autofix'"
            executeNonQuery(SQLStr, ConfigurationManager.ConnectionStrings("KFInnovaIntegrationConnectionString").ConnectionString)
            PopUpMessage("Process Completed")
        Catch ex As Exception
            PopUpMessage(ex.Message.ToString)
        End Try
    End Sub
    Protected Sub Button_SP_PO_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SP_PO_Refresh.Click
        GridView_SP_PO.DataBind()
        UpdatePanel_SP_PO.Update()
    End Sub
    Protected Sub Button_SP_SO_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SP_SO_Refresh.Click
        GridView_SP_SO_Header.DataBind()
        UpdatePanel_SP_SO.Update()
    End Sub
    Protected Sub Button_SP_TF_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SP_TF_Refresh.Click
        GridView_SP_TF.DataBind()
        UpdatePanel_SP_TF.Update()
    End Sub
    Protected Sub Button_SSIS_PO_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SSIS_PO_Refresh.Click
        GridView_SSIS_PO.DataBind()
        UpdatePanel_SSIS_PO.Update()
    End Sub
    Protected Sub Button_SSIS_SO_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SSIS_SO_Refresh.Click
        GridView_SSIS_SO.DataBind()
        UpdatePanel_SSIS_SO.Update()
    End Sub
    Protected Sub LinkButton_ItgrImport_Reset_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = btn.NamingContainer
        Dim pk As String = GridView_ITGr_Import.DataKeys(row.RowIndex).Value.ToString
        Dim SQLstr = "Update itgr_imports set importstatus=0 where id=" & pk
        executeNonQuery(SQLstr, ConfigurationManager.ConnectionStrings("KettyleInnovaConnectionString").ConnectionString)

        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub
    Protected Sub Button_Autofix_Refresh_Click(sender As Object, e As EventArgs) Handles Button_Autofix_Refresh.Click
        GridView_ITGr_Import.DataBind()
        UpdatePanel_Itgr_Import.Update()
    End Sub

    Protected Sub Button_SP_GetPackLotFromSI_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SP_GetPackLotFromSI_Refresh.Click
        GridView_PacksFromRetails.DataBind()
        UpdatePanel_GetPackFromRetail.Update()
    End Sub

    Protected Sub Button_PacksFromRetails_Click(sender As Object, e As EventArgs) Handles Button_PacksFromRetails.Click
        Try
            executeNonQuery("exec GetPacksFrom_RetailInnova_to_KettyleInnova " & TextBox_RetailsSO.Text.Trim, ConfigurationManager.ConnectionStrings("KettyleInnovaConnectionString").ConnectionString)
            PopUpMessage("Process Completed")
        Catch ex As Exception
            PopUpMessage(ex.Message)
        End Try

    End Sub

    Protected Sub Button_SP_SI_Refresh_Click(sender As Object, e As EventArgs) Handles Button_SP_SI_Refresh.Click
        GridView_SP_SI.DataBind()
        UpdatePanel_SP_SI.Update()
    End Sub
End Class
