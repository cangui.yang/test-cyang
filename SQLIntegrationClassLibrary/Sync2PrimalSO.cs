﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SQLIntegrationClassLibrary
{
  public class Sync2PrimalSO
    {
        public string UpdateSaleOrderLine (string domain, string username, string password , string SalesOrderNo
                                            , string ProductCode, string UOMC , decimal Quanlity, decimal ConsignedQuantity
                                            , decimal QtytoSihp , decimal QtytoInvoice,string LocationCode)
        {
            try
            {
                NetworkCredential Credentials = new NetworkCredential(username, password, domain);
                PrimalSalesOrderWS.SalesOrderWS SaleLineUpdate = new PrimalSalesOrderWS.SalesOrderWS();
                SaleLineUpdate.Credentials = Credentials;
                SaleLineUpdate.Library_UpdateSalesLine(SalesOrderNo, ProductCode, UOMC, Quanlity, ConsignedQuantity, QtytoSihp, QtytoInvoice, LocationCode);
                SaleLineUpdate.Dispose();
                return "Success";
            }
            catch(Exception err)
            {
                return err.Message;
            }
            
        }
        public string CreateOrderLine (string domain, string username, string password, string SalesOrderNo
                                            , string ProductCode, string UOMC, decimal Quanlity, decimal ConsignedQuantity
                                            , decimal QtytoSihp, decimal QtytoInvoice, string LocationCode)
        {
            try
            {
                NetworkCredential Credentials = new NetworkCredential(username, password, domain);
                PrimalSalesOrderWS.SalesOrderWS SaleLineUpdate = new PrimalSalesOrderWS.SalesOrderWS();
                SaleLineUpdate.Credentials = Credentials;
                SaleLineUpdate.Library_CreateSalesLine(SalesOrderNo, ProductCode, UOMC, Quanlity, ConsignedQuantity, QtytoSihp, QtytoInvoice, LocationCode);
                SaleLineUpdate.Dispose();
                return "Success";
            }
            catch (Exception err)
            {
                return err.Message;
            }


        }


    }
}
